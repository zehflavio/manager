    <div id="wrapper">    
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="./"><img src="images/logo.png" /></a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">                
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <?=$_SESSION['auth']['email'];?>
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li>
                            <a href="usuarios/formulario/<?=$_SESSION['auth']['id'];?>"><i class="fa fa-user fa-fw"></i> Meus dados</a>
                        </li>
                        <li>
                            <a href="usuarios/formulario/<?=$_SESSION['auth']['id'];?>"><i class="fa fa-key fa-fw"></i> Alterar Senha</a>
                        </li>
                        <li>
                            <a href="logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">                        
                        <li>
                            <a href="index"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="estacionamentos"><i class="fa fa-map-pin fa-fw"></i> Estacionamentos</a>
                        </li>
                        <li>
                            <a href="clientes"><i class="fa fa-users fa-fw"></i> Clientes</a>
                        </li>
                        <li>
                            <a href="usuarios"><i class="fa fa-user fa-fw"></i> Usuários</a>
                        </li>
                        <li>
                            <a href="veiculos"><i class="fa fa-car fa-fw"></i> Veículos</a>
                        </li>
                        <li>
                            <a href="avaliacoes"><i class="fa fa-star-half-o fa-fw"></i> Avaliações</a>
                        </li>
                        <li>
                            <a href="indicacoes"><i class="fa fa-sign-in fa-fw"></i> Indicações</a>
                        </li>
                        <li>
                            <a href="pagamentos"><i class="fa fa-usd fa-fw"></i> Pagamentos</a>
                        </li>
                        <li>
                            <a href="eventos"><i class="fa fa-location-arrow fa-fw"></i> Estac. Realizados</a>
                        </li>
                        <li>
                            <a href="repasses"><i class="fa fa-money fa-fw"></i> Repasses</a>
                        </li>
                        <li>
                            <a href="nfes"><i class="fa fa-file fa-fw"></i> NFes</a>
                        </li>
                        <li>
                            <a href="site-aeroportos"><i class="fa fa-plane fa-fw"></i> Site > Aeroportos</a>
                        </li>
                        <li>
                            <a href="site-eventos"><i class="fa fa-glass fa-fw"></i> Site > Eventos</a>
                        </li>
                        <!-- <li>
                            <a href="pagamentos-site"><i class="fa fa-usd fa-fw"></i> Site > Pagamentos</a>
                        </li> -->
                        <li>
                            <a href="site-repasses"><i class="fa fa-money fa-fw"></i> Site > Repasses</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>            
            <!-- /.navbar-static-side -->
        </nav>