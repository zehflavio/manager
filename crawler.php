<?php
use \ForceUTF8\Encoding;

ini_set('display_errors', '1');
error_reporting(E_ALL & ~E_NOTICE);

class Parkme {

        private $host        = '52.67.29.90';
        // private $host        = '127.0.0.1';
        private $hostplaces  = 'linceapp.com.br';
        private $dbname      = 'api';
        private $dbnameplace = 'svc_places';
        private $user        = 'root';
        private $pass        = 'q1w2e3r4';

        public function __construct () {
            $this->mysqli_api = mysqli_connect($this->host,$this->user,$this->pass,$this->dbname) or die($this->mysqli_api->connect_error);
            $this->mysqli_api->set_charset("utf8");

            $this->mysqli_places = mysqli_connect($this->hostplaces,$this->user,$this->pass,$this->dbnameplace) or die($this->mysqli_places->connect_error);
            $this->mysqli_places->set_charset("utf8");
        }

    public function crawl($url)
    {
        $jsonText  = file_get_contents($url);
        $jsonText  = str_replace('_jqjsp(', '', $jsonText);
        $jsonText  = substr($jsonText, 0, -1);
        $jsonArray = json_decode($jsonText, true);

        $estacionamentos = [];
        
        $count = 1;
        
        foreach ($jsonArray['result'] as $key => $value) {
            $estacionamentos = [];
            
            // Tem campos na tabela
            $estacionamentos['nome']         = str_replace("'", '`', $value['name']);
            $estacionamentos['longitude']    = $value['point']['coordinates'][0];
            $estacionamentos['latitude']     = $value['point']['coordinates'][1];
            
            $sql = "
                select
                    id
                from
                    api.estacionamentos est
                where
                    est.nome like \"%".$estacionamentos['nome']."%\" and
                    est.latitude  =  \"".$estacionamentos['latitude']."\" and
                    est.longitude =  \"".$estacionamentos['longitude']."\"
                ";

            $sth     = $this->mysqli_api->query($sql);

            if (!$sth) {
                die($this->mysqli_api->error);
            }

            if($sth->fetch_assoc() == null)
            {
                //Valores padroes
                $estacionamentos['usuarios_id']   = 2;
                $estacionamentos['tipo']          = 5;
                $estacionamentos['status']        = 1;
                $estacionamentos['aceita_moto']   = 0;
                $estacionamentos['aceita_carro']  = 1;
                $estacionamentos['aceita_grande'] = 0;
                $estacionamentos['dt_criacao']    = date('Y-m-d H:i:s');

                $endereco                       = $value['navigationAddress']['street'];
                $posicao                        = strpos($endereco, ', ');
                $estacionamentos['numero']      = '';

                $estacionamentos['cidade']        = $value['navigationAddress']['city'];
                $estacionamentos['estado']        = $value['navigationAddress']['state'];
                $estacionamentos['telefone']      = isset($value['phone'])? $value['phone'] : '' ;
                $estacionamentos['n_vagas']       = ($value['spacesTotal'] != '')? $value['spacesTotal'] : 0;
                
                // // sera salvo no lugar do cep
                $estacionamentos['cep']        = $value['navigationAddress']['postal'];
                
                // Separar numero do logradouro
                if($posicao != FALSE)
                {
                    $estacionamentos['numero']      = substr($endereco, $posicao+2, strlen($endereco));
                    $estacionamentos['endereco']    = str_replace('\'','`',substr($endereco, 0, $posicao));
                }
                else
                    $estacionamentos['endereco']    = str_replace('\'', '`', $endereco);   

                
                foreach ($value['hrs'] as $h => $hora) {

                    $posicao  = strpos($hora, ': ');
                    $dias     = substr($hora, 0, $posicao);
                    
                    if($dias == 'Mon-Fri'){
                        $horario = substr($hora, $posicao+1, strlen($hora));
                        $posicao = strpos($hora,'-');

                        $horario = str_replace('h','', $horario);

                        $horario_de  = substr($horario, 1,$posicao-2);

                        if(strlen($horario_de) == 1)
                            $horario_de = '0'.$horario_de.':00';
                        else if(strlen($horario_de) == 2)
                            $horario_de = $horario_de.':00'; 
                        else if(strlen($horario_de) == 3)
                            $horario_de = '0'.$horario_de;
                        
                        $estacionamentos['horario_de'] = $horario_de;

                        $horario_ate = substr($horario, $posicao, strlen($horario)) ;
                        $horario_ate = str_replace(':00-','', $horario_ate);
                        $horario_ate = str_replace('-','', $horario_ate);

                        if(strlen($horario_ate) == 1)
                            $horario_ate = '0'.$horario_ate.':00';
                        else if(strlen($horario_ate) == 2)
                            $horario_ate = $horario_ate.':00'; 
                        else if(strlen($horario_ate) == 3)
                            $horario_ate = '0'.$horario_ate;

                        $estacionamentos['horario_ate'] = $horario_ate;
                    }
                    else if($dias == 'Mon-Sun'){

                        if(strpos($hora,'24 Hours'))
                        {
                            $estacionamentos['horario_de']      = '00:01';
                            $estacionamentos['horario_de_sab']  = '00:01';
                            $estacionamentos['horario_de_dom']  = '00:01';
                            $estacionamentos['horario_ate']     = '23:59';
                            $estacionamentos['horario_ate_sab'] = '23:59';
                            $estacionamentos['horario_ate_dom'] = '23:59';
                        }else
                        {
                            $horario = substr($hora, $posicao+1, strlen($hora));
                            $posicao = strpos($hora,'-');

                            $horario = str_replace('h','', $horario);

                            $horario_de  = substr($horario, 1,$posicao-2);

                            if(strlen($horario_de) == 1)
                                $horario_de = '0'.$horario_de.':00';
                            else if(strlen($horario_de) == 2)
                                $horario_de = $horario_de.':00'; 
                            else if(strlen($horario_de) == 3)
                                $horario_de = '0'.$horario_de;
                            
                            $estacionamentos['horario_de'] = $horario_de;
                            $estacionamentos['horario_de_sab'] = $horario_de;
                            $estacionamentos['horario_de_dom'] = $horario_de;

                            $horario_ate = substr($horario, $posicao, strlen($horario)) ;
                            $horario_ate = str_replace(':00-','', $horario_ate);
                            $horario_ate = str_replace('-','', $horario_ate);

                            if(strlen($horario_ate) == 1)
                                $horario_ate = '0'.$horario_ate.':00';
                            else if(strlen($horario_ate) == 2)
                                $horario_ate = $horario_ate.':00'; 
                            else if(strlen($horario_ate) == 3)
                                $horario_ate = '0'.$horario_ate;

                            $estacionamentos['horario_ate'] = $horario_ate;
                            $estacionamentos['horario_ate_sab'] = $horario_ate;
                            $estacionamentos['horario_ate_dom'] = $horario_ate;
                        }
                    }
                    else if($dias == 'Sat'){
                        $horario = substr($hora, $posicao+1, strlen($hora));
                        
                        $posicao = strpos($horario,'-');

                        $horario = str_replace('h','', $horario);
                        
                        $horario_de  = substr($horario, 1,$posicao-2);

                        if(strlen($horario_de) == 1)
                            $horario_de = '0'.$horario_de.':00';
                        else if(strlen($horario_de) == 2)
                            $horario_de = $horario_de.':00'; 
                        else if(strlen($horario_de) == 3)
                            $horario_de = '0'.$horario_de;
                        
                        $estacionamentos['horario_de_sab'] = $horario_de;

                        $horario_ate = substr($horario, $posicao, strlen($horario)) ;
                        $horario_ate = str_replace(':00-','', $horario_ate);
                        $horario_ate = str_replace('-','', $horario_ate);

                        if(strlen($horario_ate) == 1)
                            $horario_ate = '0'.$horario_ate.':00';
                        else if(strlen($horario_ate) == 2)
                            $horario_ate = $horario_ate.':00'; 
                        else if(strlen($horario_ate) == 3)
                            $horario_ate = '0'.$horario_ate;

                        $estacionamentos['horario_ate_sab'] = $horario_ate;
                    }

                }
                
                $valores = '';

                foreach ($value['rateCard'] as $i => $precos) {

                    // if($precos != 'Permit Required' && strpos($precos, 'Free') === false ){
                    if( strpos($precos, 'R$') !== false ){
                        
                        $posicao  = strpos($precos, 'R$');
                        $preco    = str_replace('$', '',substr($precos, 0, $posicao));


                        $valor = number_format(substr($precos, $posicao+2, strlen($precos)), 2, '.', '');

                        if($valor == NULL){
                            print_r('</br>ERRRO -'.$precos.'</br>'.$estacionamentos['nome'].'</br>');
                        }

                        if($preco == '1 Hour: ' ||  $preco == '1 HOUR: ' ){
                            $valores['60'] = $valor ;
                        }else if($preco == '2 Hours: ' ||  $preco == '2 HOURS: ' ){
                            $valores['120'] = $valor ;
                        }else if($preco == "Each Add'l Hour: "){
                            $valores['A'] = $valor ;
                        }else if($preco == '30 Min: '){
                            $valores['30'] = $valor ;
                        }else if($preco == 'Monthly: ' || $preco == 'MONTHLY: ')
                        {
                            if(strpos($precos, '-R$'))
                            {
                                $posicao = strpos($precos, '-R$');
                                $valor   = number_format(substr($precos, $posicao+3, strlen($precos)), 2, '.', '');
                            }
                            $valores['MEN'] = $valor ;
                            
                        }else if($preco == 'Daily Max: ' || $preco == '12 Hours: '){
                            $valores['D'] = $valor  ;
                        }
                    }
                }

                $estacionamentos['valor_carro'] = json_encode($valores);
                
                foreach ($value['photos'] as $k => $data) {
                    
                    if( $data['assetTypes'][0] == 'Google Streetview' || $data['assetTypes'][1] == 'Google Streetview')
                    {
                        $estacionamentos['foto_media']    = $data['medium'];
                        $estacionamentos['foto_grande']   = $data['full'];
                        $estacionamentos['thumbnail']     = $data['thumbnail'];
                    }
                }
                
                //incluir estacionamento
                $sql = 
                    "
                        INSERT INTO api.estacionamentos 
                            (nome,usuarios_id,tipo,status, latitude,longitude,cidade,estado,endereco, numero, telefone, n_vagas,
                             cep,aceita_moto, aceita_carro, aceita_grande, horario_de, horario_ate, horario_de_sab, horario_ate_sab,
                             horario_de_dom, horario_ate_dom, valor_carro, foto_media,foto_grande, thumbnail,dt_criacao)
                        VALUES( '".$estacionamentos['nome']."', '".$estacionamentos['usuarios_id']."', '".$estacionamentos['tipo']."', 
                                '".$estacionamentos['status']."', '".$estacionamentos['latitude']."', '".$estacionamentos['longitude']."', 
                                '".$estacionamentos['cidade']."', '".$estacionamentos['estado']."', '".$estacionamentos['endereco']."', 
                                '".$estacionamentos['numero']."', '".$estacionamentos['telefone']."', '".$estacionamentos['n_vagas']."', 
                                '".$estacionamentos['cep']."', '".$estacionamentos['aceita_moto']."', '".$estacionamentos['aceita_carro']."', 
                                '".$estacionamentos['aceita_grande']."', '".$estacionamentos['horario_de']."', '".$estacionamentos['horario_ate']."', 
                                '".$estacionamentos['horario_de_sab']."', '".$estacionamentos['horario_ate_sab']."', '".$estacionamentos['horario_de_dom']."', 
                                '".$estacionamentos['horario_ate_dom']."', '".$estacionamentos['valor_carro']."', '".$estacionamentos['foto_media']."', 
                                '".$estacionamentos['foto_grande']."', '".$estacionamentos['thumbnail']."', '".$estacionamentos['dt_criacao']."' )
                    ";
                
                if ($this->mysqli_api->query($sql) != true) {
                    die($this->mysqli_api->error);
                 }else{
                    print_r('</br>Estacionamento salvo');
                 }
            }
            else
            {
                print_r('</br> - Ja existe - '.$estacionamentos['nome'].' - '.$count);
                $count ++;
            }   
        }   
    }
}

$parkme = new Parkme();
$lim = 10000;


//--------------------------------------------RIO DE JANEIRO------------------------------------------------------
//Zona Sul
print_r($parkme->crawl('https://api.parkme.com/lots?pub_id=x09e4f%24&chk=6fa0736437f9c8e2793df77a71857ea1&limit='.$lim.'&offset=0&rate_request=&paddedViewBounds=2.5&box=-23.08476697959808%7C-43.41166394300387%7C-22.848476019241865%7C-43.03787129468844&entry_time=2017-05-05T14%3A30&duration=60&locale=en&callback=_jqjsp&_1494003866682='));

//Zona Norte
print_r($parkme->crawl('https://api.parkme.com/lots?pub_id=x09e4f%24&chk=957c76f8d541c58fd9a7e21d2482b1b0&limit='.$lim.'&offset=0&rate_request=&paddedViewBounds=2.5&box=-22.997083023469706%7C-43.44912658210751%7C-22.760638959275923%7C-43.07533393379208&entry_time=2017-05-05T14%3A30&duration=60&locale=en&callback=_jqjsp&_1494004137636='));

//Zona Leste
print_r($parkme->crawl('https://api.parkme.com/lots?pub_id=x09e4f%24&chk=906382946234896bf0780b853d6e4315&limit='.$lim.'&offset=0&rate_request=&paddedViewBounds=2.5&box=-23.01372742009457%7C-43.75763014499819%7C-22.777312375787282%7C-43.38383749668276&entry_time=2017-05-05T15%3A00&duration=60&locale=en&callback=_jqjsp&_1494006514953='));

//Zona Oeste
print_r($parkme->crawl('https://api.parkme.com/lots?pub_id=x09e4f%24&chk=398f4bfd3d8d1f157d22717c9b560d5c&limit='.$lim.'&offset=0&rate_request=&paddedViewBounds=2.5&box=-23.05568485861164%7C-43.81861016330873%7C-22.819343056821204%7C-43.444817514993304&entry_time=2017-05-05T15%3A00&duration=60&locale=en&callback=_jqjsp&_1494006578978='));

//Centro
print_r($parkme->crawl('https://api.parkme.com/lots?pub_id=x09e4f%24&chk=67eeb2166c1b6abe04543240e027a22f&limit='.$lim.'&offset=0&rate_request=&paddedViewBounds=2.5&box=-23.041898990548596%7C-43.40130876858217%7C-22.805533109598493%7C-43.02751612026674&entry_time=2017-05-05T15%3A00&duration=60&locale=en&callback=_jqjsp&_1494006651131='));

//Outras
print_r($parkme->crawl('https://api.parkme.com/lots?pub_id=x09e4f%24&chk=7aff76baac094e6502ccb9bc4220341a&limit='.$lim.'&offset=0&rate_request=&paddedViewBounds=2.5&box=-22.951670052981434%7C-43.487311118435684%7C-22.715146911997536%7C-43.113518470120255&entry_time=2017-05-05T15%3A00&duration=60&locale=en&callback=_jqjsp&_1494009967608='));

print_r($parkme->crawl('https://api.parkme.com/lots?pub_id=x09e4f%24&chk=819dda8e681c00a9a083d692926b27ef&limit='.$lim.'&offset=0&rate_request=&paddedViewBounds=2.5&box=-23.08675395166513%7C-43.555975669216934%7C-22.85046646716598%7C-43.182183020901505&entry_time=2017-05-05T15%3A00&duration=60&locale=en&callback=_jqjsp&_1494010043690='));

print_r($parkme->crawl('https://api.parkme.com/lots?pub_id=x09e4f%24&chk=8db32d518f377b4c172526f19b5c1258&limit='.$lim.'&offset=0&rate_request=&paddedViewBounds=2.5&box=-23.066223606874786%7C-43.41332506496889%7C-22.829900221897113%7C-43.03953241665346&entry_time=2017-05-05T15%3A00&duration=60&locale=en&callback=_jqjsp&_1494010098405='));

//Niteroi
print_r($parkme->crawl('https://api.parkme.com/lots?pub_id=x09e4f%24&chk=853379c8128d778f3cedbdc4cf8376bd&limit='.$lim.'&offset=0&rate_request=&paddedViewBounds=2.5&box=-22.99205140163373%7C-43.25016092617494%7C-22.755598568629992%7C-42.87636827785951&entry_time=2017-05-05T15%3A00&duration=60&locale=en&callback=_jqjsp&_1494010506938='));

//---------------------------------------------SAO PAULO-----------------------------------------------------------
// //Zona Sul
// print_r($parkme->crawl('https://api.parkme.com/lots?pub_id=x09e4f%24&chk=dbd09861d9d952df9af8c0d7f80e88df&limit='.$lim.'&offset=0&rate_request=&paddedViewBounds=2.5&box=-23.707757439805928%7C-46.849180051033045%7C-23.58353050645439%7C-46.475387402717615&entry_time=2017-04-19T15%3A30&duration=60&locale=en&callback=_jqjsp&_1492625809580='));
// print_r('</br>');
// //Zona Norte
// print_r($parkme->crawl('https://api.parkme.com/lots?pub_id=x09e4f%24&chk=ac696cff769cac6ec4e2cb7993f958bb&limit='.$lim.'&offset=0&rate_request=&paddedViewBounds=2.5&box=-23.54703328987584%7C-46.85599661713411%7C-23.41202142820503%7C-46.48220396881868&entry_time=2017-04-25T18%3A00&duration=1500&locale=en&callback=_jqjsp&_1493067539134='));
// print_r('</br>');
// //Zona Leste
// print_r($parkme->crawl('https://api.parkme.com/lots?pub_id=x09e4f%24&chk=5ccb9f2fff8f6eade07586f89ce89f22&limit='.$lim.'&offset=0&rate_request=&paddedViewBounds=2.5&box=-23.59940293023064%7C-46.69150069123077%7C-23.46444475760933%7C-46.31770804291534&entry_time=2017-04-25T18%3A00&duration=1500&locale=en&callback=_jqjsp&_1493067663702='));
// print_r('</br>');
// //Zona Oeste
// print_r($parkme->crawl('https://api.parkme.com/lots?pub_id=x09e4f%24&chk=28fbd3f836f8a7ad5564494ed8043198&limit='.$lim.'&offset=0&rate_request=&paddedViewBounds=2.5&box=-23.64071187931014%7C-46.890223778541554%7C-23.50579613605113%7C-46.516431130226124&entry_time=2017-04-25T18%3A00&duration=1500&locale=en&callback=_jqjsp&_1493067709654='));
// print_r('</br>');
// //Centro
// print_r($parkme->crawl('https://api.parkme.com/lots?pub_id=x09e4f%24&chk=b644157ccd78c67a108f6f7bf3f73fc1&limit='.$lim.'&offset=0&rate_request=&paddedViewBounds=2.5&box=-23.61227791335919%7C-46.827155150230396%7C-23.477332957390693%7C-46.45336250191497&entry_time=2017-04-25T18%3A00&duration=1500&locale=en&callback=_jqjsp&_1493067766131='));
?>
