<?php
session_start();
require 'vendor/autoload.php';
const ENDPOINT = 'http://ec2-52-67-29-90.sa-east-1.compute.amazonaws.com/api';
const PATHIMG  = '../../api';
if(!isset($_SESSION['auth']) && end(explode('/',$_SERVER['REQUEST_URI'])) != 'login'){
    if(strpos(ENDPOINT, 'localhost') !== false){
        header("location: http://localhost/lince_manager/login");
    }else{
        header("location: //{$_SERVER['HTTP_HOST']}/manager/login");
    }
}else if(isset($_POST['email']) && isset($_POST['senha']) && end(explode('/',$_SERVER['REQUEST_URI'])) == 'login'){
    $headers = ['Content-Type' => 'application/json', 'TokenLogado' => '$2y$10$jdtsD.LcdG4oFrXf6KES9OcynWl7Fm/N15qj2Z1MY27ri3Lf82ylm'];
    $data = $_POST;
    $body = Unirest\Request\Body::json($data);
    $post = Unirest\Request::post(ENDPOINT.'/Usuarios/authManager', $headers, $body);
    $return = json_decode($post->raw_body,TRUE);
    
    if(isset($return)){
        if($return['status'] == false){
            echo '<div class="alert alert-danger"><strong>Erro!</strong> '.$return['return'].'</div>';
        }else{
            $_SESSION['auth'] = $return['usuario'];
            header('location: .');
        }
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Lince Manager</title>

    <base href="http://ec2-52-67-29-90.sa-east-1.compute.amazonaws.com/manager/" />

    <!-- Bootstrap Core CSS -->
    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link href="images/favicon.png" rel="shortcut icon" />

    <!-- script para o texteditor -->
    
    <!-- <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'textarea' });</script> -->

</head>

<body>
    <div id="wrapper">
