<?php
include 'header.php';
include 'navbar.php';

$headers = ['Content-Type' => 'application/json', 'TokenLogado' => '$2y$10$jdtsD.LcdG4oFrXf6KES9OcynWl7Fm/N15qj2Z1MY27ri3Lf82ylm'];
//$data 	 = [];
//$body = Unirest\Request\Body::json($data);
$nfes = Unirest\Request::get(ENDPOINT.'/Nfe', $headers, null)->body->return;
?>
	<div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Nfes
	            </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Número</th>
                                        <th>Cliente</th>
                                        <th>Chave</th>
                                        <th>Protocolo</th>
                                        <th>Valor</th>
                                        <th>Status</th>
                                        <th>Data</th>
                                        <th>Danfe</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	<?php
                                	$i = 0;
                                	foreach ($nfes as $key => $value) {
                                        
                                        $transacao = Unirest\Request::get(ENDPOINT.'/Pagamentos/'.$nfes->{$key}->transacoes_id, $headers, null)->body;
                                        $evento    = Unirest\Request::get(ENDPOINT.'/Eventos/'.$transacao->eventos_id, $headers, null)->body;
                                        $usuario   = Unirest\Request::get(ENDPOINT.'/Usuarios/'.$evento->usuarios_id, $headers, null)->usuario;

                                        switch ($nfes->{$key}->status) {                                            
                                            case 0:  $status = "Não gerada"; break;
                                            case 1:  $status = "Gerada"; break;
                                            case 2:  $status = "Cancelada"; break;
                                		}
                                	?>
                                		<tr class="<?=$i%2 == 0 ? 'odd' : 'even';?>">                                        
                                            <td><?=$nfes->{$key}->numero;?></td>
	                                        <td><?=$usuario->nome;?></td>
	                                        <td><?=$nfes->{$key}->chave;?></td>
                                            <td><?=$nfes->{$key}->protocolo;?></td>
	                                        <td>R$ <?=number_format($transacao->valor, 2, ',', '');?></td>
                                            <td><?=$status;?></td>                                 
                                            <td><?=date('d/m/Y H:i', strtotime($nfes->{$key}->dt_criacao));?></td>
                                            <td class="text-center"><a href="<?=ENDPOINT.'/public/NF-'.$nfes->{$key}->id.'.pdf';?>" target="_blank">NF-<?=$nfes->{$key}->id;?></a></td>
	                                    </tr>
                                	<?php
                                		$i++;
                                	}
                                    ?>
                                </tbody>
                            </table>
                        </div>                            
                    </div>                        
                </div>                    
            </div>                
        </div>
    </div>
<?php
include 'footer.php';
?>