<?php
include 'header.php';
include 'navbar.php';

$headers = ['Content-Type' => 'application/json', 'TokenLogado' => '$2y$10$jdtsD.LcdG4oFrXf6KES9OcynWl7Fm/N15qj2Z1MY27ri3Lf82ylm'];
//$data 	 = [];
//$body = Unirest\Request\Body::json($data);
$indicacoes = Unirest\Request::get(ENDPOINT.'/Indicacoes', $headers, null)->body->return;
?>
	<div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Indicações
	            </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Data</th>
                                        <th>Nome</th>
                                        <th>Telefone</th>
                                        <th>CEP</th>
                                        <th>Logradouro</th>          
                                        <th>Num</th>
                                        <th>Bairro</th>
                                        <th>Cidade</th>
                                        <th>UF</th>                     
                                    </tr>
                                </thead>
                                <tbody>
                                	<?php
                                	$i = 0;
                                	foreach ($indicacoes as $key => $value) {
                                	?>
                                		<tr class="<?=$i%2 == 0 ? 'odd' : 'even';?>">
                                            <td><?=date('d/m/Y H:i', strtotime($indicacoes->{$key}->dt_criacao));?></td>	                                        
	                                        <td><?=$indicacoes->{$key}->nome;?></td>
                                            <td><?=$indicacoes->{$key}->telefone;?></td>
                                            <td><?=$indicacoes->{$key}->cep;?></td>
                                            <td><?=$indicacoes->{$key}->logradouro;?></td>
                                            <td><?=$indicacoes->{$key}->numero;?></td>
                                            <td><?=$indicacoes->{$key}->bairro;?></td>
                                            <td><?=$indicacoes->{$key}->cidade;?></td>
                                            <td><?=$indicacoes->{$key}->estado;?></td>
	                                    </tr>
                                	<?php
                                		$i++;
                                	}
                                    ?>
                                </tbody>
                            </table>
                        </div>                            
                    </div>                        
                </div>                    
            </div>                
        </div>
    </div>
<?php
include 'footer.php';
?>