<?php
include 'header.php';
include 'navbar.php';
$headers = ['Content-Type' => 'application/json', 'TokenLogado' => '$2y$10$jdtsD.LcdG4oFrXf6KES9OcynWl7Fm/N15qj2Z1MY27ri3Lf82ylm'];
if(!empty($_POST)){
	$data = $_POST;	
	if($_FILES['thumb']['error'] != 4) {
			$data['files']['thumb'] =	Unirest\Request\Body::file(
											$_FILES['thumb']['tmp_name'], 
											$_FILES['thumb']['type'], 
											$_FILES['thumb']['name']
										);	
	}
	if($_FILES['capa']['error'] != 4) {
			$data['files']['capa'] =	Unirest\Request\Body::file(
											$_FILES['capa']['tmp_name'], 
											$_FILES['capa']['type'], 
											$_FILES['capa']['name']
										);	
	}
	$body = Unirest\Request\Body::json($data);
	if(@$_GET['id']){
		$post = Unirest\Request::post(ENDPOINT.'/SiteAeroportos/update/'.$_GET['id'], $headers, $body);
	}else{
		$post = Unirest\Request::post(ENDPOINT.'/SiteAeroportos/', $headers, $body);
	}
	$return = json_decode($post->raw_body,TRUE);
}
if(isset($_GET['id'])){
	$edit = Unirest\Request::get(ENDPOINT.'/SiteAeroportos/'.$_GET['id'], $headers, null)->body->return;
}
?>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				<?php
					echo !isset($_GET['id']) ? 'Adicionar novo' : 'Editar';
				?>
			</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6">
			<div class="panel panel-default">
				<?php
				if(isset($return)){
					if($return['status'] == false){
						echo '<div class="alert alert-danger"><strong>Erro!</strong> '.$return['return'].'</div>';
					}else{
						echo '<div class="alert alert-success"><strong>Sucesso!</strong> '.$return['return'].'</div>';
					}
				}
				?>
				<div class="panel-body panel-form">
					<form method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Nome:</label>
							<input type="text" name="nome" class="form-control" required value="<?=@$edit->nome;?>" />
						</div>
						<div class="form-group">
							<label>Localização:</label>
							<input type="text" name="localizacao" class="form-control" required value="<?=@$edit->localizacao;?>"/>
						</div>
						<div class="form-group">
							<label>Vagas:</label>
							<input type="number" min="0" name="n_vagas" class="form-control" required value="<?=@$edit->n_vagas;?>" />
						</div>
						<div class="form-group">
							<label>Valor diária:</label>
							<input type="text" name="valor_diaria" class="form-control" placeholder="0.00" required value="<?=@$edit->valor_diaria;?>" />
						</div>
                        <div class="form-group">
							<label>Valor mensal:</label>
							<input type="text" name="valor_mensal" class="form-control" placeholder="0.00" required value="<?=@$edit->valor_mensal;?>" />
						</div>
                        <div class="form-group">
							<label>Valor pacote:</label>
							<input type="text" name="valor_pacote" class="form-control" placeholder="0.00" required value="<?=@$edit->valor_pacote;?>" />
						</div>
						<div class="form-group">
							<label>Endereço:</label>
							<input type="text" name="endereco" class="form-control" required placeholder="Rua, numero - Cidade - Estado" value="<?=@$edit->endereco;?>" />
						</div>
						<div class="form-group">
							<label>Descrição:</label>
							<textarea name="descricao" class="form-control"><?=@$edit->descricao;?></textarea>
						</div>
						<div class="form-group">
							<label>Capa:</label>
							<input type="file" name="capa" class="form-control" />
							<?php
								if(!empty(@$edit->capa))
									echo '<img src="'.PATHIMG.'/uploads/site/'.@$edit->capa.'" width="100" />';
							?>
						</div>						
						<div class="form-group">
							<label>Thumb:</label>
							<input type="file" name="thumb" class="form-control" />
							<?php
								if(!empty(@$edit->thumb))
									echo '<img src="'.PATHIMG.'/uploads/site/'.@$edit->thumb.'" width="100" />';
							?>
						</div>
						<div class="form-group">
							<label>Status:</label><br /><br />
							<input type="radio" required name="status" value="1" <?=@$edit->status == 1 ? 'checked' : '';?>/>&nbsp;Ativo&nbsp;&nbsp;
							<input type="radio" required name="status" value="0" <?=@$edit->status == 0 ? 'checked' : '';?>/>&nbsp;Inativo
						</div>
						<div class="form-group text-right">
							<button type="submit" class="btn btn-success">Salvar</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>



<?php
include 'footer.php';
?>