<?php
include 'header.php';
include 'navbar.php';
$headers = ['Content-Type' => 'application/json', 'TokenLogado' => '$2y$10$jdtsD.LcdG4oFrXf6KES9OcynWl7Fm/N15qj2Z1MY27ri3Lf82ylm'];
if(isset($_GET['id'])){
	$edit = Unirest\Request::get(ENDPOINT.'/PagamentosSite/'.$_GET['id'], $headers, null)->body->return;
}
switch ($edit->status) {
    case 0:  $status = "Transação criada"; break;
    case 1:  $status = "Transação em Andamento"; break;
    case 2:  $status = "Transação Autenticada"; break;
    case 3:  $status = "Transação não Autenticada"; break;
    case 4:  $status = "Transação Autorizada"; break;
    case 5:  $status = "Transação não Autorizada"; break;
    case 6:  $status = "Transação Capturada"; break;
    case 9:  $status = "Transação Cancelada"; break;
    case 10: $status = "Transação em Autenticação"; break;
    case 13: $status = "Transação em Cancelamento"; break;
    case 13: $status = "Erro"; break;
    default: $status = "Status inválido";
}
?>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				Detalhe
			</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6">
			<div class="panel panel-default">
				<div class="panel-body panel-form">
					<div class="form-group">
						<label>Nome:</label><br />
						<?=@$edit->nome;?> <?=@$edit->sobrenome;?>
					</div>
					<div class="form-group">
						<label>E-mail:</label><br />
						<?=@$edit->email;?>
					</div>
					<div class="form-group">
						<label>Nome reserva:</label><br />
						<?=@$edit->nome_reserva;?> <?=@$edit->sobrenome_reserva;?>
					</div>
					<div class="form-group">
						<label>E-mail reserva:</label><br />
						<?=@$edit->email_reserva;?>
					</div>
					<div class="form-group">
						<label>Telefone:</label><br />
						<?=@$edit->telefone;?>
					</div>
					<div class="form-group">
						<label>Cartão:</label><br />
						<?=@$edit->cartao;?>
					</div>
					<div class="form-group">
						<label>Nome cartão:</label><br />
						<?=@$edit->nome_cartao;?>
					</div>
					<div class="form-group">
						<label>Data nascimento:</label><br />
						<?=@$edit->data_nascimento;?>
					</div>
					<div class="form-group">
						<label>CPF:</label><br />
						<?=@$edit->cpf;?>
					</div>
					<div class="form-group">
						<label>CEP:</label><br />
						<?=@$edit->cep;?>
					</div>
					<div class="form-group">
						<label>Endereço:</label><br />
						<?=@$edit->endereco;?>
					</div>
					<div class="form-group">
						<label>Número:</label><br />
						<?=@$edit->numero;?>
					</div>
					<div class="form-group">
						<label>Complemento:</label><br />
						<?=@$edit->complemento;?>
					</div>
					<div class="form-group">
						<label>Bairro:</label><br />
						<?=@$edit->bairro;?>
					</div>
					<div class="form-group">
						<label>Cidade:</label><br />
						<?=@$edit->cidade;?>
					</div>
					<div class="form-group">
						<label>UF:</label><br />
						<?=@$edit->uf;?>
					</div>
					<div class="form-group">
						<label>Quantidade:</label><br />
						<?=@$edit->quantidade;?>
					</div>
					<div class="form-group">
						<label>Valor:</label><br />
						R$ <?=number_format($edit->valor, 2, ',', '');?>
					</div>
					<div class="form-group">
						<label>TID:</label><br />
						<?=@$edit->tid;?>
					</div>
					<div class="form-group">
						<label>Status:</label><br />
						<?=@$status;?>
					</div>
					<div class="form-group">
						<label>Data:</label><br />
						<?=date('d/m/Y H:i', strtotime(@$edit->dt_criacao));?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include 'footer.php';
?>