<?php
include 'header.php';
include 'navbar.php';

$headers = ['Content-Type' => 'application/json', 'TokenLogado' => '$2y$10$jdtsD.LcdG4oFrXf6KES9OcynWl7Fm/N15qj2Z1MY27ri3Lf82ylm'];
$avaliacoes = Unirest\Request::post(ENDPOINT.'/Avaliacoes/totalHoje', $headers, null)->body->return[0]->total;
$eventos    = Unirest\Request::post(ENDPOINT.'/Eventos/totalHoje', $headers, null)->body->return[0]->total;
$aguardando = Unirest\Request::post(ENDPOINT.'/Estacionamentos/aguardando', $headers, null)->body->return;
$transacoes = Unirest\Request::post(ENDPOINT.'/Pagamentos/totalHoje', $headers, null)->body->return[0]->total;
?>
	<div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Dashboard	                
	            </h1>
            </div>
        </div>
        <div class="row">    
            <div class="cursor-click col-lg-3 col-md-6">  
                <div class="panel panel-<?php echo $avaliacoes > 0 ? 'green' : 'red';?>">
                    <a href="avaliacoes">
                        <div class="panel-heading">                        
                            <div class="row">
                                <div class="col-xs-2">
                                    <i class="fa fa-users fa-3x"></i>
                                </div>
                                <div class="col-xs-10 text-right">
                                    <div class="huge"><?=$avaliacoes;?></div>
                                    <div>Avaliações feitas hoje</div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="cursor-click col-lg-3 col-md-6">
                <div class="panel panel-<?php echo $aguardando > 0 ? 'green' : 'red';?>">
                    <a href="estacionamentos">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-2">
                                    <i class="fa fa-gears fa-3x"></i>
                                </div>
                                <div class="col-xs-10 text-right">
                                    <div class="huge"><?=$aguardando;?></div>
                                    <div>Aguardando ativação</div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="cursor-click col-lg-3 col-md-6">
                <div class="panel panel-<?php echo $eventos > 0 ? 'green' : 'red';?>">
                    <a href="eventos">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-2">
                                    <i class="fa fa-car fa-3x"></i>
                                </div>
                                <div class="col-xs-10 text-right">
                                    <div class="huge"><?=$eventos;?></div>
                                    <div>Serviços feitos hoje</div>
                                </div>
                            </div>
                        </div>
                    </a>    
                </div>
            </div>
            <div class="cursor-click col-lg-3 col-md-6">
                <div class="panel panel-<?php echo $transacoes > 0 ? 'green' : 'red';?>">
                    <a href="pagamentos">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-2">
                                    <i class="fa fa-money fa-3x"></i>
                                </div>
                                <div class="col-xs-10 text-right">
                                    <div class="huge "><?=$transacoes;?></div>
                                    <div>Pagamentos hoje</div>
                                </div>
                            </div>
                        </div>
                    </a>    
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div id="map" style="height: 400px;"></div>
                        <!--
                        <ui-gmap-window show="infoWindow.show" coords="infoWindow.coords" isIconVisibleOnClick="false" options="infoWindow.options">
                            <div>
                                <h4>{{ estacionamentoSelecionado.Nome }}</h4>
                                <div ng-controller="InfoWindowCtrl">
                                    <a class="btn" ng-click="verEstacionamentoSelecionado()" >Mais informações</a>
                                </div>
                            </div>
                        </ui-gmap-window>
                        -->
                    </div>                        
                </div>                    
            </div>                
        </div>
    </div>
<?php
include 'footer.php';
?>