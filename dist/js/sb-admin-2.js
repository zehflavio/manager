$(function() {
    $('#side-menu').metisMenu();
});

endpoint = '/api';

$(function() {
    $(window).bind("load resize", function() {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }
        height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            //$("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    $('input[name="cep"],input.cep').mask('99999-999');
    $('input.hora').mask('99:99');
    $('input.data').mask('99/99/9999');
    $('input[name="telefone"],input.telefone').mask('(99) 9999-9999?9');
    $('input[name="placa"]').mask('aaa9999',{placeholder:"aaa9999"});
    $('input[name="documento"].cpf, input.cpf').mask('999.999.999-99');
    $('input[name="documento"].cnpj, input.cnpj').mask('99.999.999/9999-99');

    $('.repassar').on('click', function(){
        var id = $(this).data('id');
        $.ajax({
            url: endpoint+'/eventos/repassar',
            headers: { 'Content-Type': 'application/json', 'TokenLogado' : '$2y$10$jdtsD.LcdG4oFrXf6KES9OcynWl7Fm/N15qj2Z1MY27ri3Lf82ylm' },
            data: JSON.stringify({'repasses_id':id}),
            type: 'POST',
            dataType: 'json',
            success: function(response){
                if(response.status == true){
                    window.location.reload();
                }else{
                    alert('Ocorreu um erro ao atualizar.');
                }
            }
        });
    });

    $('.repassar-site-eventos').on('click', function(){
        var id = $(this).data('id');
        $.ajax({
            url: endpoint+'/SiteEventos/repassar',
            headers: { 'Content-Type': 'application/json', 'TokenLogado' : '$2y$10$jdtsD.LcdG4oFrXf6KES9OcynWl7Fm/N15qj2Z1MY27ri3Lf82ylm' },
            data: JSON.stringify({'repasses_id':id}),
            type: 'POST',
            dataType: 'json',
            success: function(response){
                if(response.status == true){
                    window.location.reload();
                }else{
                    alert('Ocorreu um erro ao atualizar.');
                }
            }
        });
    });

    $('.cancelar-transacao').on('click', function(){
        var origem = $(this).data('origem'),
            id     = $(this).data('id'); 
        $.ajax({
            url: endpoint+'/pagamentos/cancelarTransacao',
            headers: { 'Content-Type': 'application/json', 'TokenLogado' : '$2y$10$jdtsD.LcdG4oFrXf6KES9OcynWl7Fm/N15qj2Z1MY27ri3Lf82ylm' },            
            type: 'POST',
            data: JSON.stringify({ 'id' : id, 'origem' : origem }),
            success: function(response){
                alert(response);
                window.location.reload();
            }
        });
    });

    $('.teste-requisao').on('click', function(){
        var tid = $(this).data('tid');
        $.ajax({
            url: endpoint+'/pagamentos/testeConsultaCielo',
            headers: { 'Content-Type': 'application/json', 'TokenLogado' : '$2y$10$jdtsD.LcdG4oFrXf6KES9OcynWl7Fm/N15qj2Z1MY27ri3Lf82ylm' },            
            type: 'POST',
            data: JSON.stringify({ 'id' : tid}),
            success: function(response){
                alert(response);
            }
        });
    });

    $('.excluir').on('click', function(){
        var entidade = $(this).data('entidade'),
            id = $(this).data('id'); 
        $.ajax({
            url: endpoint+'/'+entidade+'/'+id,
            headers: { 'Content-Type': 'application/json', 'TokenLogado' : '$2y$10$jdtsD.LcdG4oFrXf6KES9OcynWl7Fm/N15qj2Z1MY27ri3Lf82ylm' },            
            type: 'DELETE',
            dataType: 'json',
            success: function(response){
                if(response.status == true){
                    window.location.reload();
                }else{
                    alert('Ocorreu um erro ao atualizar.');
                }
            }
        });
    });

    var url = window.location;
    var element = $('ul.nav a').filter(function() {
        return this.href == url || url.href.indexOf(this.href) == 0;
    }).addClass('active').parent().parent().addClass('in').parent();
    if (element.is('li')) {
        element.addClass('active');
    }


    var ctnMap = document.getElementById('map');
    var mapOpt = { 
                    zoom: 12,
                    center: new google.maps.LatLng(-19.926280,-43.940018),                    
                 };

    if(ctnMap != undefined){
        map = new google.maps.Map(ctnMap, mapOpt);
        obj  = {};
        info = {};
        infoWindow = new google.maps.InfoWindow();
        $.ajax({
            url: endpoint+'/estacionamentos/buscarPorStatus',
            headers: { 'Content-Type': 'application/json', 'TokenLogado' : '$2y$10$jdtsD.LcdG4oFrXf6KES9OcynWl7Fm/N15qj2Z1MY27ri3Lf82ylm' },            
            type: 'POST',
            data: {status: "1"},
            dataType: 'json',
            success: function(response){                                
                proccessMarkers(response.return);
            },
            error: function(response){
                alert('Ocorreu um erro ao obter os estacionamentos.');
            }
        });
    }
});


String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

getMarkerIcon = function(tipoEstacionamento){    
    switch(parseInt(tipoEstacionamento)){
        case 1: 
            return 'images/marker_tempo.png'; 
        break;
        case 2:
            return 'images/marker_garagem.png'; 
        break;
        case 3:
            return 'images/marker_simples.png'; 
        break;
    }
};

proccessMarkers = function(estacionamentos){
    console.log(estacionamentos);
    var infoWindowContent = [];
    for(var index in estacionamentos){
        var item  = estacionamentos[index];

        infoWindowContent[index] = getInfoWindowDetails(item);
        
        var location = new google.maps.LatLng(item.latitude, item.longitude);

        marker = new google.maps.Marker({
            position    : location,
            map         : map,
            title       : item.nome,
            icon        : getMarkerIcon(item.tipo),
        });

        google.maps.event.addListener(marker, 'click', (function(marker, index){
            return function(){
                infoWindow.setContent(infoWindowContent[index]);
                infoWindow.open(map, marker);
            }
        })(marker,index));
    }
}

function getInfoWindowDetails(item){
    var contentString = '<center><h4>'+item.nome+'</h4><a href="estacionamentos/formulario/'+item.id+'" class="btn">Ver mais informações</a></center>';
    return contentString;
}
