<?php
use \ForceUTF8\Encoding;

ini_set('display_errors', '1');
error_reporting(E_ALL & ~E_NOTICE);

class Relatorio {

        private $host   = '52.67.29.90';
        private $dbname = 'api';
        private $dbnameplace = 'places';
        private $user   = 'root';
        private $pass   = 'q1w2e3r4';


        public function __construct () {
        $this->mysqli_api = mysqli_connect($this->host,$this->user,$this->pass,$this->dbname) or die($this->mysqli_api->connect_error);
        $this->mysqli_api->set_charset("utf8");

        $this->mysqli_places = mysqli_connect($this->host,$this->user,$this->pass,$this->dbnameplace) or die($this->mysqli_places->connect_error);
        $this->mysqli_places->set_charset("utf8");
    }


    public function relatorioEventos()
    {
        $sql = "
                        select
                                id,name, DATE_FORMAT(expiration_date,'%d/%m/%Y') as expiration_date , lat, lng,
                                DATE_FORMAT(date_start,'%d/%m/%Y %h:%i') as date_start,
                                DATE_FORMAT(date_end,'%d/%m/%Y %h:%i') as date_end
                        from
                                places.places
                        where
                                expiration_date >= \"".date('Y-m-d')."\" AND
                    city like \"%Belo Horizonte%\" AND
                            is_event = 1
			order by id asc";

        $sth     = $this->mysqli_places->query($sql);

        if (!$sth) {
            die($this->mysqli_places->error);
         }

         $tabela = "
            <table>
                <tr>
		   <th>Id</th>
                    <th>Evento</th>
                    <th>Data Expiração Evento</th>
                    <th>Data Inicio</th>
                    <th>Data Final</th>
                    <th>Estacionamentos</th>
                    <th>Distancia</th>
                    <th>Nome</th>
                    <th>Email</th>
                    <th>Endereço</th>
                    <th>Funcionamento</th>
                    <th>Horario dia de semana</th>
                    <th>Horario Sabado</th>
                    <th>Horario Domingo</th>
                </tr>";

        $eventos = [];
        $estacionamentos = [];

        while ($a = $sth->fetch_assoc()) {
                $eventos[] = $a;

                $sql ="
                    select
                        est.id,est.nome as estacionamento,usu.nome, est.telefone, est.endereco, usu.email,
                        ( 1.2 * 6371 * acos( cos( radians( ".$a['lat'].") ) * cos( radians( latitude ) ) * cos( radians(longitude ) - radians(".$a['lng'].") ) + sin( radians(".$a['lat'].") ) * sin( radians( latitude ) ) ) )
                        AS distance,
                        dia_de, dia_ate, horario_de, horario_ate, horario_de_sab, horario_ate_sab, horario_de_dom, horario_ate_dom
                    from
                        api.estacionamentos est
                        inner join api.usuarios usu on usu.id = est.usuarios_id
                    where
                        est.status = 1 AND est.tipo = 1
                    having distance <= 0.750";

            $sth1     = $this->mysqli_api->query($sql);
            if (!$sth1) {
                die($this->mysqli_api->error);
             }

            while ($b = $sth1->fetch_assoc()) {
                $estacionamentos[] = $b;

                $tabela .="<tr>";
 		$tabela .=" <td>".$a['id']."</td>";
                $tabela .=" <td>".$a['name']."</td>";
                $tabela .=" <td>".$a['expiration_date']."</td>";
                $tabela .=" <td>".$a['date_start']."</td>";
                $tabela .=" <td>".$a['date_end']."</td>";
                $tabela .=" <td>".$b['estacionamento']."</td>";
                $tabela .=" <td>".number_format($b['distance'],2,'.',',')."</td>";
                $tabela .=" <td>".$b['nome']."</td>";
                $tabela .=" <td>".$b['email']."</td>";
                $tabela .=" <td>".$b['endereco']."</td>";
                $tabela .=" <td>De ".$b['dia_de']." até ".$b['dia_ate']."</td>";
                $tabela .=" <td>De ".$b['horario_de']." até ".$b['horario_ate']."</td>";
                $tabela .=" <td>De ".$b['horario_de_sab']." até ".$b['horario_ate_sab']."</td>";
                $tabela .=" <td>De ".$b['horario_de_dom']." até ".$b['horario_ate_dom']."</td>";
                $tabela .="</tr>";
            }
        }

        $tabela .="</table>";

        // Determina que o arquivo é uma planilha do Excel
       header("Content-type: application/vnd.ms-excel");

       // Força o download do arquivo
       header("Content-type: application/force-download");

       // Seta o nome do arquivo
       header ("Content-Disposition: attachment; filename=Relatorio_Eventos_".date('d-m-Y').".xls" );

       header("Pragma: no-cache");

       echo $tabela;
    }
}

$relatorio = new Relatorio();

$relatorio->relatorioEventos();

?>
