<?php
use \ForceUTF8\Encoding;

ini_set('display_errors', '1');
error_reporting(E_ALL & ~E_NOTICE);

class Relatorio {

        private $host        = '52.67.29.90';
        private $hostplaces  = 'linceapp.com.br';
        private $dbname      = 'api';
        private $dbnameplace = 'gyodai';
        private $user        = 'root';
        private $pass        = 'q1w2e3r4';

        public function __construct () {
            $this->mysqli_api = mysqli_connect($this->host,$this->user,$this->pass,$this->dbname) or die($this->mysqli_api->connect_error);
            $this->mysqli_api->set_charset("utf8");

            $this->mysqli_places = mysqli_connect($this->hostplaces,$this->user,$this->pass,$this->dbnameplace) or die($this->mysqli_places->connect_error);
            $this->mysqli_places->set_charset("utf8");
        }


    public function relatorioEventos()
    {
        $sql = "
                select
                    id, DATE_FORMAT(created_at,'%d/%m/%Y') as created_at,
                    name, DATE_FORMAT(expiration_date,'%d/%m/%Y') as expiration_date , lat, lng, 
                    event_url, event_start as event_start_ori,
                    DATE_FORMAT(event_start,'%d/%m/%Y') as event_start,
                    DATE_FORMAT(event_start,'%H:%i')    as event_start_hour ,
                    DATE_FORMAT(event_end,'%d/%m/%Y')   as event_end,
                    DATE_FORMAT(event_end,'%H:%i')      as event_end_hour
                from
                    events
                where
                    event_start != '' AND       
                    expiration_date != '' AND
                    expiration_date >= \"".date('Y-m-d')."\" AND
                    expiration_date <= \"".date('Y-m-d', strtotime('+30 days'))."\" AND
                    (city like \"%Belo Horizonte%\" OR city like \"%São Paulo%\")
                order by created_at asc";
                
        $sth = $this->mysqli_places->query($sql);

        if (!$sth) {
            die($this->mysqli_places->error);
        }

         $tabela = "
            <table>
                <tr>
                    <th>Data Criacao</th>
                    <th>Evento</th>
                    <th>Data Inicio</th>
                    <th>Hora Inicio</th>
                    <th>Data Fim Evento</th>
                    <th>Hora Fim Evento</th>
                    <th>Horario dia de semana</th>
                    <th>Horario Sabado</th>
                    <th>Horario Domingo</th>
                    <th>URL Evento</th>
                    <th>Estacionamentos</th>
                    <th>Distancia</th>
                    <th>Nome</th>
                    <th>Email</th>
                    <th>Endereço</th>
                </tr>";

        $eventos = [];
        $estacionamentos = [];

        while ($a = $sth->fetch_assoc()) {
            
            $dia_funcionamento = '';

            if(isset($a['event_start_ori'])){
                
                $dia_semana = date('w', strtotime($a['event_start_ori']));
                
                if($dia_semana > 0 && $dia_semana < 6)
                {   
                    $horario_de  = "horario_de";
                    $horario_ate = "horario_ate";
                }elseif($dia_semana == 0)
                {
                    $horario_de  = "horario_de_dom";
                    $horario_ate = "horario_ate_dom";
                }elseif($dia_semana == 6)
                {
                    $horario_de  = "horario_de_sab";
                    $horario_ate = "horario_ate_sab";
                }
                
                $dia_funcionamento .= "AND \"".date('H:i', strtotime('-60 minute', strtotime($a['event_start_hour'])))."\" between ".$horario_de." AND ".$horario_ate."
                                       AND ".$horario_de." is not null 
                                       AND ".$horario_de." != \"\" ";
                                       // print_r($dia_funcionamento);die();
            }
            

            $eventos[] = $a;

            $sql ="
                select
                    est.id,est.nome as estacionamento,usu.nome, est.telefone, est.endereco, usu.email,
                    ( 1.2 * 6371 * acos( cos( radians( ".$a['lat'].") ) * cos( radians( latitude ) ) * cos( radians(longitude ) - radians(".$a['lng'].") ) + sin( radians(".$a['lat'].") ) * sin( radians( latitude ) ) ) )
                    AS distance, horario_de, horario_ate, horario_de_sab, horario_ate_sab, horario_de_dom, horario_ate_dom
                from
                    api.estacionamentos est,
                    api.usuarios usu
                where
                    est.status = 1 AND est.tipo = 1 AND
                    usu.id = est.usuarios_id $dia_funcionamento
                having distance <= 0.400";

            $sth1     = $this->mysqli_api->query($sql);
            if (!$sth1) {
                die($this->mysqli_api->error);
             }

            while ($b = $sth1->fetch_assoc()) {
                $estacionamentos[] = $b;

                $tabela .="<tr>";
                $tabela .=" <td>".$a['created_at']."</td>";
                $tabela .=" <td>".$a['name']."</td>";
                $tabela .=" <td>".$a['event_start']."</td>";
                $tabela .=" <td>".$a['event_start_hour']."</td>";
                $tabela .=" <td>".$a['event_end']."</td>";
                $tabela .=" <td>".$a['event_end_hour']."</td>";
                $tabela .=" <td>De ".$b['horario_de']." até ".$b['horario_ate']."</td>";
                $tabela .=" <td>De ".$b['horario_de_sab']." até ".$b['horario_ate_sab']."</td>";
                $tabela .=" <td>De ".$b['horario_de_dom']." até ".$b['horario_ate_dom']."</td>";
                $tabela .=" <td>".$a['event_url']."</td>";
                $tabela .=" <td>".$b['estacionamento']."</td>";
                $tabela .=" <td>".number_format($b['distance'],2,'.',',')."</td>";
                $tabela .=" <td>".$b['nome']."</td>";
                $tabela .=" <td>".$b['email']."</td>";
                $tabela .=" <td>".$b['endereco']."</td>";
                $tabela .="</tr>";
            }       
        }

       $tabela .="</table>";

        // Determina que o arquivo é uma planilha do Excel
       header("Content-type: application/vnd.ms-excel");

       // Força o download do arquivo
       header("Content-type: application/force-download");

       // Seta o nome do arquivo
       header ("Content-Disposition: attachment; filename=Relatorio_Eventos_".date('d-m-Y').".xls" );

       header("Pragma: no-cache");

       echo $tabela;
    }
}

$relatorio = new Relatorio();

$relatorio->relatorioEventos();

?>
