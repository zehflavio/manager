<?php
include 'header.php';
include 'navbar.php';
$headers = ['Content-Type' => 'application/json', 'TokenLogado' => '$2y$10$jdtsD.LcdG4oFrXf6KES9OcynWl7Fm/N15qj2Z1MY27ri3Lf82ylm'];
if(!empty($_POST)){
	$data = $_POST;	
	if($_FILES['thumb']['error'] != 4) {
			$data['files']['thumb'] =	Unirest\Request\Body::file(
											$_FILES['thumb']['tmp_name'], 
											$_FILES['thumb']['type'], 
											$_FILES['thumb']['name']
										);	
	}
	if($_FILES['capa']['error'] != 4) {
			$data['files']['capa'] =	Unirest\Request\Body::file(
											$_FILES['capa']['tmp_name'], 
											$_FILES['capa']['type'], 
											$_FILES['capa']['name']
										);	
	}
	$data['estacionamentos_id'] = implode(',',$data['estacionamentos_id']);
	$body = Unirest\Request\Body::json($data);
	if(@$_GET['id']){
		$post = Unirest\Request::post(ENDPOINT.'/SiteEventos/update/'.$_GET['id'], $headers, $body);
	}else{
		$post = Unirest\Request::post(ENDPOINT.'/SiteEventos/', $headers, $body);
	}
	$return = json_decode($post->raw_body,TRUE);
}

$estacionamentos = Unirest\Request::post(ENDPOINT.'/Estacionamentos/afiliados', $headers, null)->body->return;

if(isset($_GET['id'])){
	$edit = Unirest\Request::get(ENDPOINT.'/SiteEventos/'.$_GET['id'], $headers, null)->body->return;
}
?>
<!-- sCRIPT PARA O FROALA -->
<!-- <script type="text/javascript">
	
	$(function() {
  $('textarea#froala-editor').froalaEditor()
});

</script> -->
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				<?php
					echo !isset($_GET['id']) ? 'Adicionar novo' : 'Editar';
				?>
			</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6">
			<div class="panel panel-default">
				<?php
				if(isset($return)){
					if($return['status'] == false){
						echo '<div class="alert alert-danger"><strong>Erro!</strong> '.$return['return'].'</div>';
					}else{
						echo '<div class="alert alert-success"><strong>Sucesso!</strong> '.$return['return'].'</div>';
					}
				}
				?>
				<div class="panel-body panel-form">
					<form method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Slug:</label>
							<input type="text" name="slug" class="form-control" required value="<?=@$edit->slug;?>" />
						</div>
						<div class="form-group">
							<label>Nome:</label>
							<input type="text" name="nome" class="form-control" required value="<?=@$edit->nome;?>" />
						</div>
						<div class="form-group">
							<label>Estacionamentos:</label>
							<select name="estacionamentos_id[]" multiple class="form-control" >
								<?php
								echo '<option value="">--</option>';
								foreach ($estacionamentos as $item) {
									echo '<option value="'.$item->id.'" ';
									echo in_array($item->id, explode(',',$edit->estacionamentos_id)) ? 'selected="selected"' : null;
									echo '>'.$item->nome.'</option>';
								}
								?>
							</select>
						</div>
						<div class="form-group">
							<label>Estabelecimento:</label>
							<input type="text" name="estabelecimento" class="form-control" required value="<?=@$edit->estabelecimento;?>" />
						</div>
                        <div class="form-group">
							<label>E-mail estacionamento:</label>
							<input type="text" name="email" class="form-control" required value="<?=@$edit->email;?>" />
						</div>
						<div class="form-group">
							<label>Data:</label>
							<input type="text" name="data" class="form-control data" placeholder="dd/mm/yyyy" required value="<?=@$edit->data;?>" />
						</div>
						<div class="form-group">
							<label>Hora:</label>
							<input type="text" name="hora" class="form-control hora" placeholder="hh:mm" required value="<?=@$edit->hora;?>" />
						</div>
						<div class="form-group">
							<label>Data Fim:</label>
							<input type="text" name="data_fim" class="form-control data" placeholder="dd/mm/yyyy"  value="<?=@$edit->data_fim;?>" />
						</div>
						<div class="form-group">
							<label>Hora Fim:</label>
							<input type="text" name="hora_fim" class="form-control hora" placeholder="hh:mm"  value="<?=@$edit->hora_fim;?>" />
						</div>
						<div class="form-group">
							<label>Localização do evento:</label>
							<input type="text" name="localizacao" class="form-control" required value="<?=@$edit->localizacao;?>"/>
						</div>
						<div class="form-group">
							<label>Vagas:</label>
							<input type="number" min="0" name="n_vagas" class="form-control" required value="<?=@$edit->n_vagas;?>" />
						</div>
						<div class="form-group">
							<label>Valor:</label>
							<input type="text" name="valor" class="form-control" placeholder="0.00" required value="<?=@$edit->valor;?>" />
						</div>
						<!--
						<div class="form-group">
							<label>Endereço:</label>
							<input type="text" name="endereco" class="form-control" required placeholder="Rua, numero - Cidade - Estado" value="<?=@$edit->endereco;?>" />
						</div>
						-->
						<div class="form-group">
							<label>Descrição:</label>
							<textarea name="descricao" class="form-control" id="froala-editor"><?=@$edit->descricao;?></textarea>
						</div>
						<div class="form-group">
							<label>Capa:</label>
							<input type="file" name="capa" class="form-control" />
							<?php
								if(!empty(@$edit->capa))
									echo '<img src="'.PATHIMG.'/uploads/site/'.@$edit->capa.'" width="100" />';
							?>
						</div>						
						<div class="form-group">
							<label>Thumb:</label>
							<input type="file" name="thumb" class="form-control" />
							<?php
								if(!empty(@$edit->thumb))
									echo '<img src="'.PATHIMG.'/uploads/site/'.@$edit->thumb.'" width="100" />';
							?>
						</div>				
						<div class="form-group">
							<label>Status:</label><br /><br />
							<input type="radio" required name="status" value="1" <?=@$edit->status == 1 ? 'checked' : '';?>/>&nbsp;Ativo&nbsp;&nbsp;
							<input type="radio" required name="status" value="0" <?=@$edit->status == 0 ? 'checked' : '';?>/>&nbsp;Inativo
							<input type="radio" required name="status" value="2" <?=@$edit->status == 2 ? 'checked' : '';?>/>&nbsp;Esgotado
						</div>
						<div class="form-group">
							<label>Sitemap:</label><br /><br />
							<input type="radio" required name="add_sitemap" value="1" <?=@$edit->add_sitemap == 1 ? 'checked' : '';?>/>&nbsp;Adicionar Sitemap&nbsp;&nbsp;
							<input type="radio" required name="add_sitemap" value="0" <?=@$edit->add_sitemap == 0 ? 'checked' : '';?>/>&nbsp;Não Adicionar Sitemap
						</div>
						<div class="form-group text-right">
							<button type="submit" class="btn btn-success">Salvar</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>



<?php
include 'footer.php';
?>