// Load plugins
var del = require('del'),
    gulp = require('gulp'),
    autoprefixer = require('gulp-autoprefixer'),
    cache = require('gulp-cache'),
    concat = require('gulp-concat'),
    imagemin = require('gulp-imagemin'),
    less = require('gulp-less'),
    livereload = require('gulp-livereload'),    
    minifycss = require('gulp-minify-css'),    
    notify = require('gulp-notify'),    
    rename = require('gulp-rename'),
    lr = require('tiny-lr'),
    server = lr();
 
// Styles
gulp.task('styles', function() {
  return gulp.src('less/*.less')
    .pipe(less())
    .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
    .pipe(concat('sb-admin-2.css'))
    .pipe(gulp.dest('dist/css'))
    .pipe(notify({ message: 'Styles task complete' }));
});
 
// Scripts
gulp.task('scripts', function() {
  return gulp.src('js/app.js')
    .pipe(concat('sb-admin-2.js'))
    .pipe(gulp.dest('dist/js'))
    .pipe(notify({ message: 'Scripts task complete' }));
});

// Clean
gulp.task('clean', function(cb) {
   del(['dist/css/*', 'dist/js/app.js'], cb) 
});
 
// Default task
gulp.task('default', ['clean'], function() {
    gulp.start('styles', 'scripts');
});
 
// Watch
gulp.task('watch', function() {
    livereload.listen();
    gulp.watch('less/*', ['styles']);
    gulp.watch('js/*', ['scripts']);    
    gulp.watch(['dist/**']).on('change', livereload.changed);
});