        
        <?php
        if(!isset($_SESSION['auth']) && end(explode('/',$_SERVER['REQUEST_URI'])) != 'login'){
            echo '<div class="sidebar-fundo"></div>';
        }
        ?>                
        <!-- /#page-wrapper -->
    </div>

    <!-- jQuery -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- gMaps -->
    <script src='//maps.googleapis.com/maps/api/js'></script>

    <!-- moment.js -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.14.1/moment.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.14.1/locale/pt-br.js"></script>

    <!-- DataTables JavaScript -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="bower_components/datatables-plugins/sorting/datetime-moment.js"></script>
    <script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <!-- Masked Input -->
    <script src="bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

    <script>
    $(window).load(function() {

        $.fn.dataTable.moment('DD/MM/YYYY HH:mm', 'pt-BR');
        $('#dataTables-time').DataTable({
            responsive: true,
            language: {
                "url": "js/pt-br.json"
            },
        });

        $('#dataTables-example').DataTable({
            responsive: true,
            language: {
                "url": "js/pt-br.json"
            },
        });

        $('#dataTables-estac').DataTable( {
            responsive: true,
            language: {
                "url": "js/pt-br.json"
            },
            initComplete: function () {
                this.api().columns().every( function () {
                    var column = this;
                    var select = $('<select><option value=""></option></select>')
                                .appendTo( $(column.footer()).empty() )
                                .on( 'change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                    );
                                    column.search( val ? '^'+val+'$' : '', true, false).draw();
                                });
                    column.data().unique().sort().each( function ( d, j ) {
                        if(column.search() === '^'+d+'$'){
                            select.append( '<option value="'+d+'" selected="selected">'+d+'</option>' )
                        } else {
                            select.append( '<option value="'+d+'">'+d+'</option>' )
                        }
                    });
                });
                $('#dataTables-estac tfoot').find('th').last().find('select').remove();
            }
        });

        $('#dataTables-realizados').DataTable( {
            responsive: true,
            language: {
                "url": "js/pt-br.json"
            },
            initComplete: function () {
                this.api().columns().every( function () {
                    var column = this;
                    var select = $('<select><option value=""></option></select>')
                                .appendTo( $(column.footer()).empty() )
                                .on( 'change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                    );
                                    column.search( val ? '^'+val+'$' : '', true, false).draw();
                                });
                    column.data().unique().sort().each( function ( d, j ) {
                        if(column.search() === '^'+d+'$'){
                            select.append( '<option value="'+d+'" selected="selected">'+d+'</option>' )
                        } else {
                            select.append( '<option value="'+d+'">'+d+'</option>' )
                        }
                    });
                });
                $('#dataTables-realizados tfoot').find('th').last().find('select').remove();
            }
        });
    });
    </script>

</body>

</html>