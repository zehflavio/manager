<?php
include 'header.php';
include 'navbar.php';

$headers = ['Content-Type' => 'application/json', 'TokenLogado' => '$2y$10$jdtsD.LcdG4oFrXf6KES9OcynWl7Fm/N15qj2Z1MY27ri3Lf82ylm'];
//$data 	 = [];
//$body = Unirest\Request\Body::json($data);
$eventos = Unirest\Request::get(ENDPOINT.'/Eventos', $headers, null)->body->return;
?>
	<div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Estac. Realizados
	            </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-realizados">
                                <thead>
                                    <tr>
                                        <th>Data</th>
                                        <th>Hora</th>
                                        <th>Cliente</th>
                                        <th>Veículo</th>
                                        <th>Estacionamento</th>
                                        <th>Valor</th>
                                        <th>Status</th>
                                        <th>Ações</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Data</th>
                                        <th>Hora</th>
                                        <th>Cliente</th>
                                        <th>Veículo</th>
                                        <th>Estacionamento</th>
                                        <th>Valor</th>
                                        <th>Status</th>
                                        <th>Ações</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                	<?php
                                	$i = 0;
                                	foreach ($eventos as $key => $value) {
                                		$estac   = Unirest\Request::get(ENDPOINT.'/Estacionamentos/'.$eventos->{$key}->estacionamentos_id, $headers, null)->body->return;
                                        $usuario = Unirest\Request::get(ENDPOINT.'/Usuarios/'.$eventos->{$key}->usuarios_id, $headers, null)->body->usuario;
                                        $veiculo = Unirest\Request::get(ENDPOINT.'/Veiculos/'.$eventos->{$key}->veiculos_id, $headers, null)->body->return;
                                        $param = Unirest\Request\Body::json(['eventos_id' => $eventos->{$key}->id]);
                                        $transacao = Unirest\Request::post(ENDPOINT.'/Pagamentos/Evento', $headers, $param)->body;
                                        
                                        switch ($eventos->{$key}->status) {                                            
                                            case 1:  $status = "Aguardando estacionamento"; break;
                                            case 2:  $status = "Aguardando finalização"; break;
                                            case 3:  $status = "Finalizado"; break;
                                            case 4:  $status = "Cancelado pelo estacionamento"; break;
                                            case 5:  $status = "Cancelado pelo cliente"; break;
                                		}
                                	?>
                                		<tr class="<?=$i%2 == 0 ? 'odd' : 'even';?>">
                                            <td><?=date('d/m/Y', strtotime($eventos->{$key}->dt_inicio));?></td>
                                            <td><?=date('H:i', strtotime($eventos->{$key}->dt_inicio));?></td>	                                        
	                                        <td><?=$usuario->nome;?></td>
	                                        <td><?=$veiculo->placa;?></td>
                                            <td><?=$estac->nome;?></td>
	                                        <td>R$ <?=number_format(@$transacao->valor, 2, ',', '');?></td>
                                            <td><?=$status;?></td>	  
                                            <td class="text-center">
                                                <a href="eventos/formulario/<?=$eventos->{$key}->id;?>">
                                                    <button type="button" class="btn btn-sm btn-outline btn-info">Editar</button>
                                                </a>
                                            </td>                                      
	                                    </tr>
                                	<?php
                                		$i++;
                                	}
                                    ?>
                                </tbody>
                            </table>
                        </div>                            
                    </div>                        
                </div>                    
            </div>                
        </div>
    </div>
<?php
include 'footer.php';
?>