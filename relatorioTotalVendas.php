<?php
use \ForceUTF8\Encoding;

ini_set('display_errors', '1');
error_reporting(E_ALL & ~E_NOTICE);

class RelatorioTotalVendas {

        private $host        = '52.67.29.90';
        private $hostplaces  = 'linceapp.com.br';
        private $dbname      = 'api';
        private $dbnameplace = 'svc_places';
        private $user        = 'root';
        private $pass        = 'q1w2e3r4';

        public function __construct () {
            $this->mysqli_api = mysqli_connect($this->host,$this->user,$this->pass,$this->dbname) or die($this->mysqli_api->connect_error);
            $this->mysqli_api->set_charset("utf8");

            $this->mysqli_places = mysqli_connect($this->hostplaces,$this->user,$this->pass,$this->dbnameplace) or die($this->mysqli_places->connect_error);
            $this->mysqli_places->set_charset("utf8");
        }


    public function relatorioTotalDeVendas()
    {
         $sql ="
            select
                est.id, est.nome, latitude, longitude, est.tipo,
                est.endereco, est.numero, est.complemento, est.bairro,
                est.cep, est.cidade, est.estado
            from
                api.estacionamentos est
            where
                est.tipo != 1 and
                (est.estado = 'MG'or
                est.estado = 'SP'or
                est.estado = 'RJ' )";  

        $sth  = $this->mysqli_api->query($sql);

        if (!$sth) {
            die($this->mysqli_api->error);
         }

         $tabela = "
            <table>
                <tr>
                    <th>Id</th>
                    <th>Nome Estacionamento</th>
                    <th>Quantidade</th>
                    <th>Distancia</th>
                    <th>Nome Evento</th>
                    <th>Endereco</th>
                    <th>Numero</th>
                    <th>Complemento</th>
                    <th>Bairro</th>
                    <th>CEP</th>
                    <th>Cidade</th>
                    <th>Estado</th>
                    <th>Data Inicio</th>
                    <th>Data Fim</th>
                    <th>Endereco Evento</th>
                    <th>Bairro Evento</th>
                    <th>Cidade Evento</th>
                </tr>";

        $estacionamentos = [];

        while ($a = $sth->fetch_assoc()) {
            
            $sql ="
                select
                    name, expiration_date, city, district, address, event_start, event_end,
                    ( 1.2 * 6371 * acos( cos( radians( ".$a['latitude'].") ) * cos( radians( lat ) ) * 
                        cos( radians(lng ) - radians(".$a['longitude'].") ) + sin( radians(".$a['latitude'].") ) * sin( radians( lat ) ) ) )
                    AS distance
                from
                    svc_places.places
                where
                    is_event = 1 AND
                    expiration_date >= \"".date('Y-m-d')."\"           
                having distance <= 0.400
                ";


            $sth1     = $this->mysqli_places->query($sql);
            
            if (!$sth1) {
                die($this->mysqli_places->error);
            }

            $cont = 0;
            while ($b = $sth1->fetch_assoc()) {
               $tabela .= '<tr>';
                    $tabela .= '<td>'.$a['id'].'</td>';
                    $tabela .= '<td>'.$a['nome'].'</td>';
                    $tabela .= '<td>'.$sth1->num_rows.'</td>';
                    $tabela .= '<td>'.$b['distance'].'</td>';
                    $tabela .= '<td>'.$b['name'].'</td>';
                    $tabela .= '<td>'.$a['endereco'].'</td>';
                    $tabela .= '<td>'.$a['numero'].'</td>';
                    $tabela .= '<td>'.$a['complemento'].'</td>';
                    $tabela .= '<td>'.$a['bairro'].'</td>';
                    $tabela .= '<td>'.$a['cep'].'</td>';
                    $tabela .= '<td>'.$a['cidade'].'</td>';
                    $tabela .= '<td>'.$a['estado'].'</td>';
                    $tabela .= '<td>'.$b['event_start'].'</td>';
                    $tabela .= '<td>'.$b['event_end'].'</td>';
                    $tabela .= '<td>'.$b['address'].'</td>';
                    $tabela .= '<td>'.$b['district'].'</td>';
                    $tabela .= '<td>'.$b['city'].'</td>';
               $tabela .= '</tr>'; 
            }
           
        }
       $tabela .="</table>";

        // Determina que o arquivo é uma planilha do Excel
       header("Content-type: application/vnd.ms-excel");

       // Força o download do arquivo
       header("Conatent-type: application/force-download");

       // Seta o nome do arquivo
       header ("Content-Disposition: attachment; filename=Relatorio_Total_Venda_".date('d-m-Y').".xls" );

       header("Pragma: no-cache");

       echo $tabela;
    }
}

$relatorio = new RelatorioTotalVendas();

$relatorio->relatorioTotalDeVendas();

?>

