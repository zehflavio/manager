<?php
use \ForceUTF8\Encoding;

ini_set('display_errors', '1');
error_reporting(E_ALL & ~E_NOTICE);

class RelatorioEstacionamento {

        private $host        = '52.67.29.90';
        private $hostplaces  = 'linceapp.com.br';
        private $dbname      = 'api';
        private $dbnameplace = 'svc_places';
        private $user        = 'root';
        private $pass        = 'q1w2e3r4';

        public function __construct () {
            $this->mysqli_api = mysqli_connect($this->host,$this->user,$this->pass,$this->dbname) or die($this->mysqli_api->connect_error);
            $this->mysqli_api->set_charset("utf8");

            $this->mysqli_places = mysqli_connect($this->hostplaces,$this->user,$this->pass,$this->dbnameplace) or die($this->mysqli_places->connect_error);
            $this->mysqli_places->set_charset("utf8");
        }


    public function relatorioEstacionamentos()
    {
         $sql ="
            select
                est.id, est.nome, latitude, longitude, est.estado,
                est.cidade, est.tipo
            from
                api.estacionamentos est
                inner join api.usuarios usu on usu.id = est.usuarios_id
            where
                est.estado = 'MG'or
                est.estado = 'SP'or
                est.estado = 'RJ' ";  

        $sth     = $this->mysqli_api->query($sql);

        if (!$sth) {
            die($this->mysqli_api->error);
         }

         $tabela = "
            <table>
                <tr>
                    <th>Id</th>
                    <th>Nome</th>
                    <th>Quantidade</th>
                    <th>Cidade</th>
                    <th>Estado</th>
                    <th>Tipo</th>
                </tr>";

        $estacionamentos = [];

        while ($a = $sth->fetch_assoc()) {

            $sql ="
                select
                    ( 1.2 * 6371 * acos( cos( radians( ".$a['latitude'].") ) * cos( radians( lat ) ) * 
                        cos( radians(lng ) - radians(".$a['longitude'].") ) + sin( radians(".$a['latitude'].") ) * sin( radians( lat ) ) ) )
                    AS distance
                from
                    svc_places.places
                where
                    expiration_date >= \"".date('Y-m-d')."\" AND          
                    is_event = 1
                having distance <= 0.400
                ";

            $sth1     = $this->mysqli_places->query($sql);

            if (!$sth1) {
                die($this->mysqli_places->error);
            }

            $contador = 0;

            while ($b = $sth1->fetch_assoc()) {
                $contador ++ ;
            }

            if($contador > 0){
                $a['quantidade'] = $contador;
                
                $tabela .="<tr>";
                    $tabela .=" <td>".$a['id']."</td>";
                    $tabela .=" <td>".$a['nome']."</td>";
                    $tabela .=" <td>".$a['quantidade']."</td>";
                    $tabela .=" <td>".$a['cidade']."</td>";
                    $tabela .=" <td>".$a['estado']."</td>";
                    $tabela .=" <td>".$a['tipo']."</td>";
                $tabela .="</tr>";
            }
        }
       $tabela .="</table>";

        // Determina que o arquivo é uma planilha do Excel
       header("Content-type: application/vnd.ms-excel");

       // Força o download do arquivo
       header("Conatent-type: application/force-download");

       // Seta o nome do arquivo
       header ("Content-Disposition: attachment; filename=Relatorio_Venda_".date('d-m-Y').".xls" );

       header("Pragma: no-cache");

       echo $tabela;
    }
}

$relatorio = new RelatorioEstacionamento();

$relatorio->relatorioEstacionamentos();

?>

