<?php
include 'header.php';
include 'navbar.php';

$headers = ['Content-Type' => 'application/json', 'TokenLogado' => '$2y$10$jdtsD.LcdG4oFrXf6KES9OcynWl7Fm/N15qj2Z1MY27ri3Lf82ylm'];
//$data 	 = [];
//$body = Unirest\Request\Body::json($data);
$estacionamentos = Unirest\Request::get(ENDPOINT.'/Estacionamentos/todosEstacionamentos', $headers, null)->body->return;
?>
	<div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Estacionamentos cadastrados
	                <div class="links-header pull-right">		                
		                <div>		                    
                            <a href="estacionamentos/formulario"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar</a>
		                </div>
		            </div>
	            </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-estac">
                                <thead>
                                    <tr>
                                        <th width="25%">Nome</th>
                                        <th>Tipo</th>                                        
                                        <th>Comissão</th>
                                        <th>Status</th>
                                        <th>Ações</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th width="25%">Nome</th>
                                        <th>Tipo</th>                                        
                                        <th>Comissão</th>
                                        <th>Status</th>
                                        <th>Ações</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                	<?php
                                	$i = 0;
                                	foreach ($estacionamentos as $key => $value) {
                                		$usuario = Unirest\Request::get(ENDPOINT.'/Usuarios/'.$estacionamentos[$key]->usuarios_id, $headers, null)->body->usuario;
                                		switch ($estacionamentos[$key]->tipo) {
                                			case 1:
                                				$tipo = 'Parceiros';
                            				break;
                                			case 2:
                                				$tipo = 'Garagem';
                            				break;
                            				case 3:
                                				$tipo = 'Simples';
                            				break;
                                    		case 4:
                                        		$tipo = 'Prospect';
                                    		break;
                                    		case 5:
                                        		$tipo = 'Não parceiro';
                                    		break;
                                		}
                                	?>
                                		<tr class="<?=$i%2 == 0 ? 'odd' : 'even';?>">
	                                        <td><?=$estacionamentos[$key]->nome;?></td>
	                                        <td><?=$tipo;?></td>
	                                        <td><?=$estacionamentos[$key]->comissao;?>%</td>
	                                        <td><?=$estacionamentos[$key]->status == 0 ? 'Inativo' : 'Ativo';?></td>
	                                        <td class="text-center">
	                                        	<a href="estacionamentos/formulario/<?=$estacionamentos[$key]->id;?>">
                                                    <button type="button" class="btn btn-sm btn-outline btn-info">Editar</button>
                                                </a>
                                                <?php if($estacionamentos[$key]->status == 1){?>
                                                  <button   type="button" 
                                                            class="btn btn-sm btn-outline btn-danger excluir" 
                                                            data-entidade="estacionamentos" 
                                                            data-id="<?=$estacionamentos[$key]->id;?>">Excluir</button>
                                                <?php } ?>
	                                        </td>
	                                    </tr>
                                	<?php
                                		$i++;
                                	}
                                    ?>
                                </tbody>
                            </table>
                        </div>                            
                    </div>                        
                </div>                    
            </div>                
        </div>
    </div>
<?php
include 'footer.php';
?>
