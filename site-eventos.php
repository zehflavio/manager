<?php
include 'header.php';
include 'navbar.php';

$headers = ['Content-Type' => 'application/json', 'TokenLogado' => '$2y$10$jdtsD.LcdG4oFrXf6KES9OcynWl7Fm/N15qj2Z1MY27ri3Lf82ylm'];
$eventos = Unirest\Request::get(ENDPOINT.'/SiteEventos', $headers, null)->body;
?>
	<div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Eventos cadastrados
	                <div class="links-header pull-right">		                
		                <div>
		                    <a href="site-eventos/formulario"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar</a>
		                </div>
		            </div>
	            </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-time">
                                <thead>
                                    <tr>
                                        <th>Data/Hora</th>
                                        <th>Nome</th>                                                                                
                                        <th>Localização</th>              
                                        <th>Status</th>
                                        <th>Ações</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	<?php
                                	$i = 0;
                                	foreach ($eventos as $key => $value) {                                		
                                	?>
                                		<tr class="<?=$i%2 == 0 ? 'odd' : 'even';?>">
                                            <td><?=$eventos->{$key}->data.' '.$eventos->{$key}->hora;?></td>
	                                        <td><?=$eventos->{$key}->nome;?></td>
                                            <td><?=$eventos->{$key}->localizacao;?></td>
                                            <td><?=$eventos->{$key}->status == 0 ? 'Inativo' : 'Ativo';?></td>
	                                        <td class="text-center">
	                                        	<a href="site-eventos/formulario/<?=$eventos->{$key}->id;?>">
                                                    <button type="button" class="btn btn-sm btn-outline btn-info">Editar</button>
                                                </a>
                                                <?php if($eventos->{$key}->status == 1){?>
                                                    <button   type="button" 
                                                              class="btn btn-sm btn-outline btn-danger excluir" 
                                                              data-entidade="site-eventos" 
                                                              data-id="<?=$eventos->{$key}->id;?>">Excluir</button>
                                                <?php } ?>
	                                        </td>
	                                    </tr>
                                	<?php
                                		$i++;
                                	}
                                    ?>
                                </tbody>
                            </table>
                        </div>                            
                    </div>                        
                </div>                    
            </div>                
        </div>
    </div>
<?php
include 'footer.php';
?>