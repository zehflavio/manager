<?php
include 'header.php';
include 'navbar.php';

$headers = ['Content-Type' => 'application/json', 'TokenLogado' => '$2y$10$jdtsD.LcdG4oFrXf6KES9OcynWl7Fm/N15qj2Z1MY27ri3Lf82ylm'];
if(isset($_GET['id'])){
	$edit = Unirest\Request::get(ENDPOINT.'/Veiculos/'.$_GET['id'], $headers, null)->body;
}
if(!empty($_POST)){
	$data = $_POST;
	$body = Unirest\Request\Body::json($data);
	if(@$_GET['id']){
		$post = Unirest\Request::post(ENDPOINT.'/Veiculos/update/'.$_GET['id'], $headers, $body);
	}else{
		$post = Unirest\Request::post(ENDPOINT.'/Veiculos/', $headers, $body);
	}
	$return = json_decode($post->raw_body,TRUE);
}
$data 	  = ['tipo' => 2];
$body     = Unirest\Request\Body::json($data);
$clientes = Unirest\Request::post(ENDPOINT.'/Usuarios/Tipo', $headers, $body)->body->return;
?>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				<?php
					echo !isset($_GET['id']) ? 'Adicionar novo' : 'Editar';
				?>
			</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6">
			<div class="panel panel-default">
				<?php
				if(isset($return)){
					if($return['status'] == false){
						echo '<div class="alert alert-danger"><strong>Erro!</strong> '.$return['return'].'</div>';
					}else{
						echo '<div class="alert alert-success"><strong>Sucesso!</strong> '.$return['return'].'</div>';
					}
				}
				?>
				<div class="panel-body panel-form">
					<form method="post" action="">
						<div class="form-group">
							<label>Cliente:</label>
							<select name="usuarios_id" class="form-control" required >
								<?php
								foreach ($clientes as $item) {
									echo '<option value="'.$item->id.'">'.$item->nome.'</option>';
								}
								?>
							</select>
						</div>
						<div class="form-group">
							<label>Placa:</label>
							<input type="text" name="placa" class="form-control" required value="<?=@$edit->placa;?>" />
						</div>
						<div class="form-group">
							<label>Tipo:</label><br /><br />
							<input type="radio" required name="tipo" value="1" <?=@$edit->tipo == 1 ? 'checked' : '';?>/>&nbsp;Moto&nbsp;&nbsp;
							<input type="radio" required name="tipo" value="2" <?=@$edit->tipo == 2 ? 'checked' : '';?>/>&nbsp;Carro&nbsp;&nbsp;
							<input type="radio" required name="tipo" value="3" <?=@$edit->tipo == 3 ? 'checked' : '';?>/>&nbsp;Veículo Grande
						</div>
						<div class="form-group">
							<label>Status:</label><br /><br />
							<input type="radio" required name="status" value="1" <?=@$edit->status == 1 ? 'checked' : '';?>/>&nbsp;Ativo&nbsp;&nbsp;
							<input type="radio" required name="status" value="0" <?=@$edit->status == 0 ? 'checked' : '';?>/>&nbsp;Inativo
						</div>
						<div class="form-group text-right">
							<button type="submit" class="btn btn-success">Salvar</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>



<?php
include 'footer.php';
?>