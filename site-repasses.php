<?php
include 'header.php';
include 'navbar.php';

$headers = ['Content-Type' => 'application/json', 'TokenLogado' => '$2y$10$jdtsD.LcdG4oFrXf6KES9OcynWl7Fm/N15qj2Z1MY27ri3Lf82ylm'];
$not = [];
$eventos_recorrentes = Unirest\Request::post(ENDPOINT.'/SiteEventos/repasses', $headers, null)->body->return;
?>
	<div id="page-wrapper">

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Repasses pendentes
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-time">
                                <thead>
                                    <tr>
                                        <th>Estacionamento</th>
                                        <th>Data da Venda</th> 
                                        <th>Total Vendas</th>
                                        <th>Total Cobrado</th>
                                        <th>Comissão</th>
                                        <th>Total à ser repassado</th>
                                        <th>Status</th>
                                        <th>Data do Repasse</th> 
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 0;
                                    foreach ($eventos_recorrentes as $key => $value) { 
                                        if($eventos_recorrentes[$key]->total_eventos == 0){
                                            continue;
                                        } 
                                        if(in_array($eventos_recorrentes[$key]->id, $not) || empty($eventos_recorrentes[$key]->id)){
                                            continue;
                                        }
                                        $repasse = round($eventos_recorrentes[$key]->total_repasses * ((100-$eventos_recorrentes[$key]->comissao) / 100), 2);
                                    ?>
                                        <tr class="<?=$i%2 == 0 ? 'odd' : 'even';?>">                                           
                                            <td><?=$eventos_recorrentes[$key]->nome;?></td>
                                            <td><?=date('d/m/Y H:i',strtotime($eventos_recorrentes[$key]->dt_criacao));?></td>
                                            <td><?=$eventos_recorrentes[$key]->total_eventos;?></td>
                                            <td>R$ <?=number_format($eventos_recorrentes[$key]->total_repasses, 2, ',', '');?></td>
                                            <td><?=$eventos_recorrentes[$key]->comissao;?>%</td>
                                            <td>R$ <?=number_format($repasse, 2, ',', '');?></td>
                                            <td><?=$eventos_recorrentes[$key]->repasse == 1 ? 'Repassado' : 'Pendente';?></td>
                                            <td><?=!empty($eventos_recorrentes[$key]->dt_repasse) ? date('d/m/Y H:i',strtotime($eventos_recorrentes[$key]->dt_repasse)) : '-';?></td>
                                            <td class="text-center">
                                                <?php
                                                if($eventos_recorrentes[$key]->repasse == 0){
                                                    echo '<button type="button" data-id="'.$eventos_recorrentes[$key]->transacao_id.'" class="btn btn-sm btn-outline btn-success repassar-site-eventos">Repassar</button>';
                                                }
                                                ?>                                                
                                            </td>
                                        </tr>
                                    <?php
                                        $i++;
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>                            
                    </div>                        
                </div>                    
            </div>                
        </div>
    </div>
<?php
include 'footer.php';
?>