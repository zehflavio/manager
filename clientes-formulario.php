<?php
include 'header.php';
include 'navbar.php';

$headers = ['Content-Type' => 'application/json', 'TokenLogado' => '$2y$10$jdtsD.LcdG4oFrXf6KES9OcynWl7Fm/N15qj2Z1MY27ri3Lf82ylm'];
if(!empty($_POST)){
	$data = $_POST;
	$body = Unirest\Request\Body::json($data);
	if(@$_GET['id']){
		$post = Unirest\Request::post(ENDPOINT.'/Usuarios/update/'.$_GET['id'], $headers, $body);
	}else{
		$post = Unirest\Request::post(ENDPOINT.'/Usuarios/', $headers, $body);
	}
	$return = json_decode($post->raw_body,TRUE);
}
if(isset($_GET['id'])){
	$edit = Unirest\Request::get(ENDPOINT.'/Usuarios/'.$_GET['id'], $headers, null)->body->usuario;
}
?>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				<?php
					echo !isset($_GET['id']) ? 'Adicionar novo' : 'Editar';
				?>
			</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6">
			<div class="panel panel-default">
				<?php
				if(isset($return)){
					if($return['status'] == false){
						echo '<div class="alert alert-danger"><strong>Erro!</strong> '.$return['return'].'</div>';
					}else{
						echo '<div class="alert alert-success"><strong>Sucesso!</strong> '.$return['return'].'</div>';
					}
				}
				?>
				<div class="panel-body panel-form">
					<form method="post" action="">
						<div class="form-group">
							<label>Nome:</label>
							<input type="text" name="nome" class="form-control" required value="<?=@$edit->nome;?>" />
						</div>
						<div class="form-group">
							<label>E-mail:</label>
							<input type="email" name="email" class="form-control" required value="<?=@$edit->email;?>"/>
						</div>
						<div class="form-group">
							<label>Senha:</label>
							<input type="password" name="senha" class="form-control" <?=!@$_GET['id'] ? 'required' : '';?>/>
						</div>
						<div class="form-group">
							<label>Telefone:</label>
							<input type="text" class="form-control" name="telefone" value="<?=@$edit->telefone;?>"/>
						</div>
						<div class="form-group">
							<label>CPF:</label>
							<input type="text" class="form-control cpf" required name="documento" value="<?=@$edit->documento;?>"/>
						</div>
						<div class="form-group">
							<label>Endereço:</label>
							<input type="text" class="form-control" required name="endereco" value="<?=@$edit->endereco;?>"/>
						</div>
						<div class="form-group">
							<label>Número:</label>
							<input type="text" class="form-control" required name="numero" value="<?=@$edit->numero;?>"/>
						</div>
						<div class="form-group">
							<label>Complemento:</label>
							<input type="text" class="form-control" name="complemento" value="<?=@$edit->complemento;?>"/>
						</div>
						<div class="form-group">
							<label>Bairro:</label>
							<input type="text" class="form-control" required name="bairro" value="<?=@$edit->bairro;?>"/>
						</div>
						<div class="form-group">
							<label>Cidade:</label>
							<input type="text" class="form-control" required name="cidade" value="<?=@$edit->cidade;?>"/>
						</div>
						<div class="form-group">
							<label>Estado:</label>
							<input type="text" class="form-control" required name="estado" value="<?=@$edit->estado;?>"/>
						</div>
						<div class="form-group">
							<label>CEP:</label>
							<input type="text" class="form-control" required name="cep" value="<?=@$edit->cep;?>"/>
						</div>
						<div class="form-group">
							<label>Tipo:</label><br /><br />
							<input type="radio" required name="tipo" value="2" <?=@$edit->tipo == 2 ? 'checked' : '';?>/>&nbsp;Cliente&nbsp;&nbsp;
							<input type="radio" required name="tipo" value="3" <?=@$edit->tipo == 3 ? 'checked' : '';?>/>&nbsp;Responsável
						</div>
						<div class="form-group">
							<label>Status:</label><br /><br />
							<input type="radio" required name="status" value="1" <?=@$edit->status == 1 ? 'checked' : '';?>/>&nbsp;Ativo&nbsp;&nbsp;
							<input type="radio" required name="status" value="0" <?=@$edit->status == 0 ? 'checked' : '';?>/>&nbsp;Inativo
						</div>
						<div class="form-group text-right">
							<button type="submit" class="btn btn-success">Salvar</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>



<?php
include 'footer.php';
?>