<?php
include 'header.php';
include 'navbar.php';

$headers = ['Content-Type' => 'application/json', 'TokenLogado' => '$2y$10$jdtsD.LcdG4oFrXf6KES9OcynWl7Fm/N15qj2Z1MY27ri3Lf82ylm'];
//$data 	 = [];
//$body = Unirest\Request\Body::json($data);
$transacoes = Unirest\Request::get(ENDPOINT.'/Pagamentos', $headers, null)->body->return;
?>
	<div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Pagamentos
	            </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Data</th>
                                        <th>Cliente</th>
                                        <th>Valor</th>
                                        <th>Status</th>
                                        <th>TID</th>                               
                                    </tr>
                                </thead>
                                <tbody>
                                	<?php
                                	$i = 0;
                                	foreach ($transacoes as $key => $value) {
                                		$evento  = Unirest\Request::get(ENDPOINT.'/Eventos/'.$transacoes->{$key}->eventos_id, $headers, null)->body->return;
                                        $usuario = Unirest\Request::get(ENDPOINT.'/Usuarios/'.$evento->usuarios_id, $headers, null)->body->usuario;

                                        switch ($transacoes->{$key}->status) {
                                            case 0:  $status = "Transação criada"; break;
                                            case 1:  $status = "Transação em Andamento"; break;
                                            case 2:  $status = "Transação Autenticada"; break;
                                            case 3:  $status = "Transação não Autenticada"; break;
                                            case 4:  $status = "Transação Autorizada"; break;
                                            case 5:  $status = "Transação não Autorizada"; break;
                                            case 6:  $status = "Transação Capturada"; break;
                                            case 9:  $status = "Transação Cancelada"; break;
                                            case 10: $status = "Transação em Autenticação"; break;
                                            case 13: $status = "Transação em Cancelamento"; break;
                                            case 13: $status = "Erro"; break;
                                            default: $status = "Status inválido";
                                		}
                                	?>
                                		<tr class="<?=$i%2 == 0 ? 'odd' : 'even';?>">
                                            <td><?=date('d/m/Y H:i', strtotime($transacoes->{$key}->dt_criacao));?></td>	                                        
	                                        <td><?=$usuario->nome;?></td>	                                        
	                                        <td>R$ <?=number_format($transacoes->{$key}->valor, 2, ',', '');?></td>
	                                        <td><?=$status;?></td>
                                            <td><?=$evento->tid;?></td>
                                            <td class="text-center">
                                                <?php if($transacoes->{$key}->status == 6){ ?>
	                                        	<button type="button" data-origem="eventos" data-id="<?=$transacoes->{$key}->id;?>" 
                                                        class="btn btn-sm btn-outline btn-danger cancelar-transacao">
                                                    Cancelar transação
                                                </button>
                                                <?php } ?>
	                                        </td>
	                                    </tr>
                                	<?php
                                		$i++;
                                	}
                                    ?>
                                </tbody>
                            </table>
                        </div>                            
                    </div>                        
                </div>                    
            </div>                
        </div>
    </div>
<?php
include 'footer.php';
?>