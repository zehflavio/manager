<?php
include 'header.php';
include 'navbar.php';

$headers = ['Content-Type' => 'application/json', 'TokenLogado' => '$2y$10$jdtsD.LcdG4oFrXf6KES9OcynWl7Fm/N15qj2Z1MY27ri3Lf82ylm'];
$aeroportos = Unirest\Request::get(ENDPOINT.'/SiteAeroportos', $headers, null)->body->return;
?>
	<div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Aeroportos cadastrados
	                <div class="links-header pull-right">		                
		                <div>
		                    <a href="site-aeroportos/formulario"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar</a>
		                </div>
		            </div>
	            </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Nome</th>                                                                                
                                        <th>Localização</th>                                        
                                        <th>Status</th>
                                        <th>Ações</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	<?php
                                	$i = 0;
                                	foreach ($aeroportos as $key => $value) {                                		
                                	?>
                                		<tr class="<?=$i%2 == 0 ? 'odd' : 'even';?>">
	                                        <td><?=$aeroportos->{$key}->nome;?></td>
                                            <td><?=$aeroportos->{$key}->localizacao;?></td>
                                            <td><?=$aeroportos->{$key}->status == 0 ? 'Inativo' : 'Ativo';?></td>
	                                        <td class="text-center">
	                                        	<a href="site-aeroportos/formulario/<?=$aeroportos->{$key}->id;?>">
                                                    <button type="button" class="btn btn-sm btn-outline btn-info">Editar</button>
                                                </a>
                                                <?php if($aeroportos->{$key}->status == 1){?>
                                                    <button   type="button" 
                                                              class="btn btn-sm btn-outline btn-danger excluir" 
                                                              data-entidade="site-aeroportos" 
                                                              data-id="<?=$aeroportos->{$key}->id;?>">Excluir</button>
                                                <?php } ?>
	                                        </td>
	                                    </tr>
                                	<?php
                                		$i++;
                                	}
                                    ?>
                                </tbody>
                            </table>
                        </div>                            
                    </div>                        
                </div>                    
            </div>                
        </div>
    </div>
<?php
include 'footer.php';
?>