<?php
include 'header.php';
include 'navbar.php';

$headers = ['Content-Type' => 'application/json', 'TokenLogado' => '$2y$10$jdtsD.LcdG4oFrXf6KES9OcynWl7Fm/N15qj2Z1MY27ri3Lf82ylm'];
$transacoes = Unirest\Request::get(ENDPOINT.'/PagamentosSite', $headers, null)->body->return;
?>
	<div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Pagamentos Site
	            </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-time">
                                <thead>
                                    <tr>
                                        <th>Data</th>
                                        <th>Evento/Aeroporto</th>
                                        <th>Cliente</th>
                                        <th>Valor</th>
                                        <th>Status</th>
                                        <th>TID</th> 
                                        <th>Voucher</th>        
                                        <th>Ações</th>               
                                    </tr>
                                </thead>
                                <tbody>
                                	<?php
                                	$i = 0;                                    
                                	foreach ($transacoes as $key => $value) {
                                        $parametros["id"] = $transacoes->{$key}->tid;
                                        $body = Unirest\Request\Body::json($parametros);
                                        $post = Unirest\Request::post(ENDPOINT.'/Pagamentos/consultaTransacao/', $headers, $body);
                                        $returnPost = json_decode($post->raw_body, TRUE);


                                        if(!empty($transacoes->{$key}->site_aeroportos_id)){
                                            $evento  = Unirest\Request::get(ENDPOINT.'/SiteAeroportos/'.$transacoes->{$key}->site_aeroportos_id, $headers, null)->body->return;
                                        }else{
                                            $evento  = Unirest\Request::get(ENDPOINT.'/SiteEventos/'.$transacoes->{$key}->site_eventos_id, $headers, null)->body->return;
                                        }
                                        $cod = $returnPost['status']!=NULL?$returnPost['status']:$transacoes->{$key}->status;

                                        switch ($cod) {
                                            case 0:  $status = "Transação criada"; break;
                                            case 1:  $status = "Transação em Andamento"; break;
                                            case 2:  $status = "Transação Autenticada"; break;
                                            case 3:  $status = "Transação não Autenticada"; break;
                                            case 4:  $status = "Transação Autorizada"; break;
                                            case 5:  $status = "Transação não Autorizada"; break;
                                            case 6:  $status = "Transação Capturada"; break;
                                            case 9:  $status = "Transação Cancelada"; break;
                                            case 10: $status = "Transação em Autenticação"; break;
                                            case 12: $status = "Transação em Cancelamento"; break;
                                            case 13: $status = "Erro"; break;
                                            default: $status = "Status inválido";
                                		}
                                	?>
                                		<tr class="<?=$i%2 == 0 ? 'odd' : 'even';?>">
                                            <td><?=date('d/m/Y H:i', strtotime($transacoes->{$key}->dt_criacao));?></td>	                                        
	                                        <td><?=$evento->nome;?></td>	
                                            <td><?=$transacoes->{$key}->nome;?></td>                                            
	                                        <td>R$ <?=number_format($transacoes->{$key}->valor, 2, ',', '');?></td>
	                                        <td>
                                                <?=$status;?>
                                            </td>
                                             
                                            <td><?=$transacoes->{$key}->tid;?>
                                            <?php if($transacoes->{$key}->tid!=""){?>
                                            <button type="button" data-tid="<?=$transacoes->{$key}->tid;?>" 
                                                        class="btn btn-sm btn-outline btn-danger teste-requisao">
                                                    Consulta Cielo
                                                </button>
                                            <?php } ?>
                                            </td>
                                            <td><?=@$transacoes->{$key}->voucher;?></td>
                                            <td class="text-center">
                                                <a href="pagamentos-site/formulario/<?=$transacoes->{$key}->id;?>">
                                                    <button type="button" class="btn btn-sm btn-outline btn-info">Ver detalhes</button>
                                                </a>
                                                <?php if($cod==6){ ?>
                                                <button type="button" data-origem="site" data-id="<?=$transacoes->{$key}->id;?>" 
                                                        class="btn btn-sm btn-outline btn-danger cancelar-transacao">
                                                    Cancelar transação
                                                </button>
                                                <?php } ?>
	                                        </td>
	                                    </tr>
                                	<?php
                                		$i++;
                                	}
                                    ?>
                                </tbody>
                            </table>
                        </div>                            
                    </div>                        
                </div>                    
            </div>                
        </div>
    </div>
    
<?php
include 'footer.php';
?>