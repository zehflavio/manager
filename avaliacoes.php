<?php
include 'header.php';
include 'navbar.php';

$headers    = ['Content-Type' => 'application/json', 'TokenLogado' => '$2y$10$jdtsD.LcdG4oFrXf6KES9OcynWl7Fm/N15qj2Z1MY27ri3Lf82ylm'];
$avaliacoes = Unirest\Request::get(ENDPOINT.'/Avaliacoes', $headers, null)->body->return;
?>
	<div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Avaliações cadastradas
	            </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>De</th>
                                        <th>Para</th>
                                        <th>Tipo</th>
                                        <th>Nota</th>
                                        <th>Data criação</th>
                                        <th>Comentário</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	<?php
                                	$i = 0;
                                	foreach ($avaliacoes as $key => $value) {            
                                        switch ($avaliacoes->{$key}->tipo) {
                                            case 1:
                                                $tipo = 'Cliente';
                                            break;
                                            case 2:
                                                $tipo = 'Estacionamento';
                                            break;
                                        }
                                        if($avaliacoes->{$key}->tipo == 1){
                                            $de   = Unirest\Request::get(ENDPOINT.'/Usuarios/'.$avaliacoes->{$key}->de, $headers, null)->body->usuario;
                                            $para = Unirest\Request::get(ENDPOINT.'/Estacionamentos/'.$avaliacoes->{$key}->para, $headers, null)->body->return;
                                        }else{
                                            $de   = Unirest\Request::get(ENDPOINT.'/Estacionamentos/'.$avaliacoes->{$key}->de, $headers, null)->body->return;
                                            $para = Unirest\Request::get(ENDPOINT.'/Usuarios/'.$avaliacoes->{$key}->para, $headers, null)->body->usuario;
                                        }
                                	?>
                                		<tr class="<?=$i%2 == 0 ? 'odd' : 'even';?>">
	                                        <td><?=$de->nome;?></td>
                                            <td><?=$para->nome;?></td>
                                            <td><?=$tipo;?></td>
	                                        <td><?=$avaliacoes->{$key}->nota;?></td>
                                            <td><?=date('d/m/Y H:i', strtotime($avaliacoes->{$key}->dt_criacao));?></td>
                                            <td><?=$avaliacoes->{$key}->comentario;?></td>
	                                    </tr>
                                	<?php
                                		$i++;
                                	}
                                    ?>
                                </tbody>
                            </table>
                        </div>                            
                    </div>                        
                </div>                    
            </div>                
        </div>
    </div>
<?php
include 'footer.php';
?>