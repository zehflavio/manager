<?php
include 'header.php';
include 'navbar.php';

$headers  = ['Content-Type' => 'application/json', 'TokenLogado' => '$2y$10$jdtsD.LcdG4oFrXf6KES9OcynWl7Fm/N15qj2Z1MY27ri3Lf82ylm'];
$data 	  = ['tipo' => 'usuarios'];
$body     = Unirest\Request\Body::json($data);
$clientes = Unirest\Request::post(ENDPOINT.'/Usuarios/Tipo', $headers, $body)->body->return;
?>
	<div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Usuários cadastrados
	                <div class="links-header pull-right">		                
		                <div>
		                   <a href="usuarios/formulario">
					 <span class="glyphicon glyphicon-plus-sign"></span> Adicionar
		                   </a>
				</div>
		            </div>
	            </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Nome</th>
                                        <th>E-mail</th>
                                        <th>Data criação</th>
                                        <th>Último login</th>                                                                                
                                        <th>Status</th>
                                        <th>Ações</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	<?php
                                	$i = 0;
                                	foreach ($clientes as $key => $value) {                                        
                                	?>
                                		<tr class="<?=$i%2 == 0 ? 'odd' : 'even';?>">
	                                        <td><?=$clientes->{$key}->nome;?></td>
	                                        <td><?=$clientes->{$key}->email;?></td>
                                            <td><?=date('d/m/Y H:i', strtotime($clientes->{$key}->dt_criacao));?></td>
                                            <td><?=date('d/m/Y H:i', strtotime($clientes->{$key}->ultimo_login));?></td>
	                                        <td><?=$clientes->{$key}->status == 0 ? 'Inativo' : 'Ativo';?></td>
	                                        <td class="text-center">
	                                        	<a href="usuarios/formulario/<?=$clientes->{$key}->id;?>">
                                                    <button type="button" class="btn btn-sm btn-outline btn-info">Editar</button>
                                                </a>
	                                        	<?php if($clientes->{$key}->status == 1){?>
                                                    <button   type="button" 
                                                            class="btn btn-sm btn-outline btn-danger excluir" 
                                                            data-entidade="usuarios" 
                                                            data-id="<?=$clientes->{$key}->id;?>">Excluir</button>
                                                <?php } ?>
	                                        </td>
	                                    </tr>
                                	<?php
                                		$i++;
                                	}
                                    ?>
                                </tbody>
                            </table>
                        </div>                            
                    </div>                        
                </div>                    
            </div>                
        </div>
    </div>
<?php
include 'footer.php';
?>
