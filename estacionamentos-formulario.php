<?php
include 'header.php';
include 'navbar.php';

$headers = ['Content-Type' => 'application/json', 'TokenLogado' => '$2y$10$jdtsD.LcdG4oFrXf6KES9OcynWl7Fm/N15qj2Z1MY27ri3Lf82ylm'];
if(isset($_GET['id'])){
	$edit = Unirest\Request::get(ENDPOINT.'/Estacionamentos/'.$_GET['id'], $headers, null)->body->return;
	//resgata conta do estacionamento
	$data 	  = ['estacionamentos_id' => $edit->id];
	$body     = Unirest\Request\Body::json($data);
	$r_conta  = Unirest\Request::post(ENDPOINT.'/Contas/estacionamento', $headers, $body)->body->return;
	$conta_id = @$r_conta->id;	
}

function get_lat_long($address) {
	$res = Unirest\Request::get('http://maps.googleapis.com/maps/api/geocode/json?address=' . $address .'&sensor=false', null, null);
	$ret = json_decode($res->raw_body, TRUE);

	if(isset($ret['results'][0]['geometry'])){
		return $ret['results'][0]['geometry']['location'];
	}else{
		return false;
	}
}

if(!empty($_POST)){	

	if(isset($_POST['excluir_fotos'])){
		foreach ($_POST['excluir_fotos'] as $value) {
			Unirest\Request::post(ENDPOINT.'/Estacionamentos/excluirImagem/', $headers, Unirest\Request\Body::json(['id' => $value]) );
		}
	}
	unset($_POST['excluir_fotos']);

	$estac 	  = $_POST['estacionamento'];

	$address  = @$estac['endereco'].',';
	$address .= @$estac['numero'].' - ';
	$address .= @$estac['cidade'].' - ';
	$address .= @$estac['estado'];

	$geo = get_lat_long(urlencode($address));
	if($geo){
		$estac['latitude']  = $geo['lat'];
		$estac['longitude'] = $geo['lng'];	
	}
	$estac['funcionamento'] = !empty($estac['funcionamento']) ? json_encode($estac['funcionamento']) : NULL;
	$estac['valor_moto'] 	= json_encode($estac['valor_moto']);
	$estac['valor_carro'] 	= json_encode($estac['valor_carro']);
	$estac['valor_grande'] 	= json_encode($estac['valor_grande']);

	//checa os arquivos para armazenas as imagens do estacionamento
	foreach ($_FILES['estacionamento']['error']['fotos'] as $k => $item) {
		if($item != 4){			
			$estac['files'][] =	Unirest\Request\Body::file(
									$_FILES['estacionamento']['tmp_name']['fotos'][$k], 
									$_FILES['estacionamento']['type']['fotos'][$k], 
									$_FILES['estacionamento']['name']['fotos'][$k]
								);
		}
	}
	$body = Unirest\Request\Body::json($estac);
	if(@$_GET['id']){
		$post   = Unirest\Request::post(ENDPOINT.'/Estacionamentos/update/'.$_GET['id'], $headers, $body);
		$return = json_decode($post->raw_body, TRUE);

		$conta = $_POST['contas_bancarias'];
		$errors = array_filter($conta);
		if (!empty($errors)) {			
			if(isset($conta_id)){
				$conta['estacionamentos_id'] = $_GET['id'];
				$ins   = Unirest\Request\Body::json($conta);
				$banco = Unirest\Request::post(ENDPOINT.'/Contas/update/'.$conta_id, $headers, $ins);
			}else{
				$conta['estacionamentos_id'] = $_GET['id'];
				$ins   = Unirest\Request\Body::json($conta);
				$banco = Unirest\Request::post(ENDPOINT.'/Contas/', $headers, $ins);
			}
		}
	}else{
		$post   = Unirest\Request::post(ENDPOINT.'/Estacionamentos/', $headers, $body);		
		$return = json_decode($post->raw_body, TRUE);
		//armazena conta do estacionamento
		$conta = $_POST['contas_bancarias'];
		$errors = array_filter($conta);
		if (!empty($errors)) {
			$conta['estacionamentos_id'] = $return['id'];
			$ins   = Unirest\Request\Body::json($conta);
			$banco = Unirest\Request::post(ENDPOINT.'/Contas/', $headers, $ins);
		}
	}
}

$data 	  = ['tipo' => 'responsaveis'];
$body     = Unirest\Request\Body::json($data);
$clientes = Unirest\Request::post(ENDPOINT.'/Usuarios/Tipo', $headers, $body)->body->return;
$estacionamentos = Unirest\Request::post(ENDPOINT.'/Estacionamentos/afiliados', $headers, null)->body->return;

if($_GET['id']){
	$edit = Unirest\Request::get(ENDPOINT.'/Estacionamentos/'.$_GET['id'], $headers, null)->body->return;
	//resgata as imagens do estacionamento
	$data 	  = ['estacionamentos_id' => $_GET['id']];
	$body     = Unirest\Request\Body::json($data);
	$imagens  = Unirest\Request::post(ENDPOINT.'/Estacionamentos/imagens', $headers, $body)->body->return;
}
?>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				<?php
					echo !isset($_GET['id']) ? 'Adicionar novo' : 'Editar';
				?>
			</h1>
		</div>
	</div>

	<form method="post" enctype="multipart/form-data">


    <div class="form-group text-right">	
		<button type="submit" class="btn btn-success">Salvar</button>
	</div>


	<div class="row">
		<div class="col-lg-6">		
			<div class="panel panel-default">
				<?php
				if(isset($return)){
					if($return['status'] == false){
						echo '<div class="alert alert-danger"><strong>Erro!</strong> '.$return['return'].'</div>';
					}else{
						echo '<div class="alert alert-success"><strong>Sucesso!</strong> '.$return['return'].'</div>';
					}
				}
				?>
				<div class="panel-heading">
                    Estacionamento
                </div>
				<div class="panel-body panel-form">
					<div class="form-group">
					<label>Responsável:</label>
						<select name="estacionamento[usuarios_id]" class="form-control" required >
							<?php
							foreach ($clientes as $item) {
								echo '<option value="'.$item->id.'" ';
								echo $item->id == @$edit->usuarios_id  ? 'selected="selected"' : null;
								echo '>'.$item->nome.'</option>';
							}
							?>
						</select>
					</div>
					<div class="form-group">
					<label>Matriz:</label>
						<select name="estacionamento[estacionamentos_id]" class="form-control" >
							<?php
							echo '<option value="">--</option>';
							foreach ($estacionamentos as $item) {
								echo '<option value="'.$item->id.'" ';
								echo $item->id == @$edit->estacionamentos_id  ? 'selected="selected"' : null;
								echo '>'.$item->nome.'</option>';
							}
							?>
						</select>
					</div>
					<div class="form-group">
						<label>Nome:</label>
						<input type="text" name="estacionamento[nome]" class="form-control" required value="<?=@$edit->nome;?>" />
					</div>
					<div class="form-group">
						<label>CNPJ:</label>
						<input type="text" class="form-control cnpj" name="estacionamento[documento]" value="<?=@$edit->documento;?>"/>
					</div>
					<div class="form-group">
						<label>Inscrição Municipal:</label>
						<input type="text" class="form-control" name="estacionamento[inscricao_municipal]" value="<?=@$edit->inscricao_municipal;?>"/>
					</div>
					<div class="form-group">
						<label>Endereço:</label>
						<input type="text" class="form-control" name="estacionamento[endereco]" value="<?=@$edit->endereco;?>"/>
					</div>
					<div class="form-group">
						<label>Número:</label>
						<input type="text" class="form-control" name="estacionamento[numero]" value="<?=@$edit->numero;?>"/>
					</div>
					<div class="form-group">
						<label>Complemento:</label>
						<input type="text" class="form-control" name="estacionamento[complemento]" value="<?=@$edit->complemento;?>"/>
					</div>
					<div class="form-group">
						<label>Bairro:</label>
						<input type="text" class="form-control" name="estacionamento[bairro]" value="<?=@$edit->bairro;?>"/>
					</div>
					<div class="form-group">
						<label>Cidade:</label>
						<input type="text" class="form-control" name="estacionamento[cidade]" value="<?=@$edit->cidade;?>"/>
					</div>
					<div class="form-group">
						<label>Estado:</label>
						<input type="text" class="form-control" name="estacionamento[estado]" value="<?=@$edit->estado;?>"/>
					</div>
					<div class="form-group">
						<label>CEP:</label>
						<input type="text" class="form-control cep" name="estacionamento[cep]" value="<?=@$edit->cep;?>"/>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="panel panel-default">
				<div class="panel-heading">
                    Funcionamento
                </div>
				<div class="panel-body panel-form">
					<div class="form-group">
						<label>Status:</label><br /><br />
						<input type="radio" required name="estacionamento[status]" value="0" <?=@$edit->status == 0 ? 'checked' : '';?>/>&nbsp;Inativo&nbsp;&nbsp;
						<input type="radio" required name="estacionamento[status]" value="1" <?=@$edit->status == 1 ? 'checked' : '';?>/>&nbsp;Ativo&nbsp;&nbsp;
						<input type="radio" required name="estacionamento[status]" value="2" <?=@$edit->status == 2 ? 'checked' : '';?>/>&nbsp;Aguardando
					</div>
					<div class="form-group">
						<label>Tipo:</label><br /><br />
						<input type="radio" required name="estacionamento[tipo]" value="1" <?=@$edit->tipo == 1 ? 'checked' : '';?>/>&nbsp;Parceiros&nbsp;&nbsp;
						<!--
						<input type="radio" required name="estacionamento[tipo]" value="2" <?=@$edit->tipo == 2 ? 'checked' : '';?>/>&nbsp;Garagem&nbsp;&nbsp;
						-->
						<input type="radio" required name="estacionamento[tipo]" value="3" <?=@$edit->tipo == 3 ? 'checked' : '';?>/>&nbsp;Simples				
						<input type="radio" required name="estacionamento[tipo]" value="4" <?=@$edit->tipo == 4 ? 'checked' : '';?>/>&nbsp;Prospect
						<input type="radio" required name="estacionamento[tipo]" value="5" <?=@$edit->tipo == 5 ? 'checked' : '';?>/>&nbsp;Não parceiro
					</div>
					<div class="form-group">
						<label>Aceita:</label><br /><br />
						<input type="checkbox" name="estacionamento[aceita_moto]"   value="1" <?=@$edit->aceita_moto == 1 ? 'checked' : '';?>/>&nbsp;Moto&nbsp;&nbsp;
						<input type="checkbox" name="estacionamento[aceita_carro]"  value="1" <?=@$edit->aceita_carro == 1 ? 'checked' : '';?>/>&nbsp;Carro&nbsp;&nbsp;
						<input type="checkbox" name="estacionamento[aceita_grande]" value="1" <?=@$edit->aceita_grande == 1 ? 'checked' : '';?>/>&nbsp;Grande
					</div>
					<div class="form-group">
						<label>Comissão %:</label>
						<input type="number" class="form-control" name="estacionamento[comissao]" min="0" value="<?=(int)@$edit->comissao;?>"/>
					</div>
					<div class="form-group">
						<label>Dias de Funcionamento:</label><br /><br />
						<?php $funcionamento = json_decode(@$edit->funcionamento);?>
						<input type="checkbox" name="estacionamento[funcionamento][Dom]" value="1" <?=@$funcionamento->Dom == 1 ? 'checked' : '';?>/>&nbsp;Dom&nbsp;
						<input type="checkbox" name="estacionamento[funcionamento][Seg]" value="1" <?=@$funcionamento->Seg == 1 ? 'checked' : '';?>/>&nbsp;Seg&nbsp;
						<input type="checkbox" name="estacionamento[funcionamento][Ter]" value="1" <?=@$funcionamento->Ter == 1 ? 'checked' : '';?>/>&nbsp;Ter&nbsp;
						<input type="checkbox" name="estacionamento[funcionamento][Qua]" value="1" <?=@$funcionamento->Qua == 1 ? 'checked' : '';?>/>&nbsp;Qua&nbsp;
						<input type="checkbox" name="estacionamento[funcionamento][Qui]" value="1" <?=@$funcionamento->Qui == 1 ? 'checked' : '';?>/>&nbsp;Qui&nbsp;
						<input type="checkbox" name="estacionamento[funcionamento][Sex]" value="1" <?=@$funcionamento->Sex == 1 ? 'checked' : '';?>/>&nbsp;Sex&nbsp;
						<input type="checkbox" name="estacionamento[funcionamento][Sab]" value="1" <?=@$funcionamento->Sab == 1 ? 'checked' : '';?>/>&nbsp;Sáb&nbsp;
					</div>
					<div class="form-group">
						<label>Hora Abertura:</label>
						<input type="text" name="estacionamento[horario_de]" class="form-control hora" value="<?=@$edit->horario_de;?>"/>
					</div>						
					<div class="form-group">
						<label>Hora Fechamento:</label>
						<input type="text" class="form-control hora" name="estacionamento[horario_ate]" value="<?=@$edit->horario_ate;?>"/>
					</div>
					<div class="form-group">
						<label>Hora Abertura Sábado:</label>
						<input type="text" name="estacionamento[horario_de_sab]" class="form-control hora" value="<?=@$edit->horario_de_sab;?>"/>
					</div>						
					<div class="form-group">
						<label>Hora Fechamento Sábado:</label>
						<input type="text" class="form-control hora" name="estacionamento[horario_ate_sab]" value="<?=@$edit->horario_ate_sab;?>"/>
					</div>
					<div class="form-group">
						<label>Hora Abertura Domingo:</label>
						<input type="text" name="estacionamento[horario_de_dom]" class="form-control hora" value="<?=@$edit->horario_de_dom;?>"/>
					</div>						
					<div class="form-group">
						<label>Hora Fechamento Domingo:</label>
						<input type="text" class="form-control hora" name="estacionamento[horario_ate_dom]" value="<?=@$edit->horario_ate_dom;?>"/>
					</div>
					<div class="form-group">
						<label>Número de Vagas:</label>
						<input type="number" class="form-control" name="estacionamento[n_vagas]" min="0" value="<?=@$edit->n_vagas;?>"/>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="panel panel-default">
				<div class="panel-heading">
                    Imagens
                </div>
				<div class="panel-body panel-form">
					<div class="form-group">
						<label>Arquivos:</label><br /><br />
						<input type="file" name="estacionamento[fotos][0]" />
						<input type="file" name="estacionamento[fotos][1]" />
						<input type="file" name="estacionamento[fotos][2]" />
						<input type="file" name="estacionamento[fotos][3]" />
						<br />
						<?php
						if(count(@$imagens) > 0){
							echo '<br /><strong>Marque para excluir</strong><br /><br />';
							foreach(@$imagens as $item){
								echo '<input type="checkbox" name="excluir_fotos[]" value="'.$item->id.'" />&nbsp;';
								echo '<img src="../../api/uploads/'.$item->estacionamentos_id.'/'.$item->arquivo.'" width="100" />';
								echo '&nbsp;&nbsp;<br /><br />';
							}
						}
						?>
					</div>					
				</div>
			</div>
		</div>
		<br clear="all"/><br clear="all"/><br clear="all"/>
		<div class="col-lg-6">
			<div class="panel panel-default">
				<div class="panel-heading">
                    Valores Moto
                </div>
                <?php $valor_moto = json_decode(@$edit->valor_moto);?>
				<div class="panel-body panel-form">					
					<div class="form-group">
						<label>15 minutos:</label>
						<input type="text" class="form-control" name="estacionamento[valor_moto][15]" value="<?=@$valor_moto->{15};?>"/>
					</div>
					<div class="form-group">
						<label>30 minutos:</label>
						<input type="text" class="form-control" name="estacionamento[valor_moto][30]" value="<?=@$valor_moto->{30};?>"/>
					</div>
					<div class="form-group">
						<label>45 minutos:</label>
						<input type="text" class="form-control" name="estacionamento[valor_moto][45]" value="<?=@$valor_moto->{45};?>"/>
					</div>
					<div class="form-group">
						<label>60 minutos:</label>
						<input type="text" class="form-control" name="estacionamento[valor_moto][60]" value="<?=@$valor_moto->{60};?>"/>
					</div>
					<div class="form-group">
						<label>120 minutos:</label>
						<input type="text" class="form-control" name="estacionamento[valor_moto][120]" value="<?=@$valor_moto->{120};?>"/>
					</div>
					<div class="form-group">
						<label>Hora Adicional:</label>
						<input type="text" class="form-control" name="estacionamento[valor_moto][A]" value="<?=@$valor_moto->{A};?>"/>
					</div>
					<div class="form-group">
						<label>12 horas:</label>
						<input type="text" class="form-control" name="estacionamento[valor_moto][12]" value="<?=@$valor_moto->{12};?>"/>
					</div>
					<div class="form-group">
						<label>24 horas:</label>
						<input type="text" class="form-control" name="estacionamento[valor_moto][24]" value="<?=@$valor_moto->{24};?>"/>
					</div>
					<div class="form-group">
						<label>Dia:</label>
						<input type="text" class="form-control" name="estacionamento[valor_moto][D]" value="<?=@$valor_moto->{D};?>"/>
					</div>
					<div class="form-group">
						<label>Pernoite:</label>
						<input type="text" class="form-control" name="estacionamento[valor_moto][PER]" value="<?=@$valor_moto->{PER};?>"/>
					</div>
					<div class="form-group">
						<label>Mensalista:</label>
						<input type="text" class="form-control" name="estacionamento[valor_moto][MEN]" value="<?=@$valor_moto->{MEN};?>"/>
					</div>
					<div class="form-group">
						<label>Mensalista Noturno:</label>
						<input type="text" class="form-control" name="estacionamento[valor_moto][MEN_NOT]" value="<?=@$valor_moto->{MEN_NOT};?>"/>
					</div>
					<div class="form-group">
						<label>Evento:</label>
						<input type="text" class="form-control" name="estacionamento[valor_moto][E]" value="<?=@$valor_moto->{E};?>"/>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="panel panel-default">
				<div class="panel-heading">
                    Valores Carro
                </div>
                <?php $valor_carro = json_decode(@$edit->valor_carro);?>
				<div class="panel-body panel-form">					
					<div class="form-group">
						<label>15 minutos:</label>
						<input type="text" class="form-control" name="estacionamento[valor_carro][15]" value="<?=@$valor_carro->{15};?>"/>
					</div>
					<div class="form-group">
						<label>30 minutos:</label>
						<input type="text" class="form-control" name="estacionamento[valor_carro][30]" value="<?=@$valor_carro->{30};?>"/>
					</div>
					<div class="form-group">
						<label>45 minutos:</label>
						<input type="text" class="form-control" name="estacionamento[valor_carro][45]" value="<?=@$valor_carro->{45};?>"/>
					</div>
					<div class="form-group">
						<label>60 minutos:</label>
						<input type="text" class="form-control" name="estacionamento[valor_carro][60]" value="<?=@$valor_carro->{60};?>"/>
					</div>
					<div class="form-group">
						<label>120 minutos:</label>
						<input type="text" class="form-control" name="estacionamento[valor_carro][120]" value="<?=@$valor_carro->{120};?>"/>
					</div>
					<div class="form-group">
						<label>Hora Adicional:</label>
						<input type="text" class="form-control" name="estacionamento[valor_carro][A]" value="<?=@$valor_carro->{A};?>"/>
					</div>
					<div class="form-group">
						<label>12 horas:</label>
						<input type="text" class="form-control" name="estacionamento[valor_carro][12]" value="<?=@$valor_carro->{12};?>"/>
					</div>
					<div class="form-group">
						<label>24 horas:</label>
						<input type="text" class="form-control" name="estacionamento[valor_carro][24]" value="<?=@$valor_carro->{24};?>"/>
					</div>
					<div class="form-group">
						<label>Dia:</label>
						<input type="text" class="form-control" name="estacionamento[valor_carro][D]" value="<?=@$valor_carro->{D};?>"/>
					</div>
					<div class="form-group">
						<label>Pernoite:</label>
						<input type="text" class="form-control" name="estacionamento[valor_carro][PER]" value="<?=@$valor_carro->{PER};?>"/>
					</div>
					<div class="form-group">
						<label>Mensalista:</label>
						<input type="text" class="form-control" name="estacionamento[valor_carro][MEN]" value="<?=@$valor_carro->{MEN};?>"/>
					</div>
					<div class="form-group">
						<label>Mensalista Noturno:</label>
						<input type="text" class="form-control" name="estacionamento[valor_carro][MEN_NOT]" value="<?=@$valor_carro->{MEN_NOT};?>"/>
					</div>
					<div class="form-group">
						<label>Evento:</label>
						<input type="text" class="form-control" name="estacionamento[valor_carro][E]" value="<?=@$valor_carro->{E};?>"/>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="panel panel-default">
				<div class="panel-heading">
                    Valores Grande
                </div>
                <?php $valor_grande = json_decode(@$edit->valor_grande);?>
				<div class="panel-body panel-form">					
					<div class="form-group">
						<label>15 minutos:</label>
						<input type="text" class="form-control" name="estacionamento[valor_grande][15]" value="<?=@$valor_grande->{15};?>"/>
					</div>
					<div class="form-group">
						<label>30 minutos:</label>
						<input type="text" class="form-control" name="estacionamento[valor_grande][30]" value="<?=@$valor_grande->{30};?>"/>
					</div>
					<div class="form-group">
						<label>45 minutos:</label>
						<input type="text" class="form-control" name="estacionamento[valor_grande][45]" value="<?=@$valor_grande->{45};?>"/>
					</div>
					<div class="form-group">
						<label>60 minutos:</label>
						<input type="text" class="form-control" name="estacionamento[valor_grande][60]" value="<?=@$valor_grande->{60};?>"/>
					</div>
					<div class="form-group">
						<label>120 minutos:</label>
						<input type="text" class="form-control" name="estacionamento[valor_grande][120]" value="<?=@$valor_grande->{120};?>"/>
					</div>
					<div class="form-group">
						<label>Hora Adicional:</label>
						<input type="text" class="form-control" name="estacionamento[valor_grande][A]" value="<?=@$valor_grande->{A};?>"/>
					</div>
					<div class="form-group">
						<label>12 horas:</label>
						<input type="text" class="form-control" name="estacionamento[valor_grande][12]" value="<?=@$valor_grande->{12};?>"/>
					</div>
					<div class="form-group">
						<label>24 horas:</label>
						<input type="text" class="form-control" name="estacionamento[valor_grande][24]" value="<?=@$valor_grande->{24};?>"/>
					</div>
					<div class="form-group">
						<label>Dia:</label>
						<input type="text" class="form-control" name="estacionamento[valor_grande][D]" value="<?=@$valor_grande->{D};?>"/>
					</div>
					<div class="form-group">
						<label>Pernoite:</label>
						<input type="text" class="form-control" name="estacionamento[valor_grande][PER]" value="<?=@$valor_grande->{PER};?>"/>
					</div>
					<div class="form-group">
						<label>Mensalista:</label>
						<input type="text" class="form-control" name="estacionamento[valor_grande][MEN]" value="<?=@$valor_grande->{MEN};?>"/>
					</div>
					<div class="form-group">
						<label>Mensalista Noturno:</label>
						<input type="text" class="form-control" name="estacionamento[valor_grande][MEN_NOT]" value="<?=@$valor_grande->{MEN_NOT};?>"/>
					</div>
					<div class="form-group">
						<label>Evento:</label>
						<input type="text" class="form-control" name="estacionamento[valor_grande][E]" value="<?=@$valor_grande->{E};?>"/>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="panel panel-default">
				<div class="panel-heading">
                    Dados bancários
                </div>
				<div class="panel-body panel-form">					
					<div class="form-group">
						<label>Titular:</label>
						<input type="text" class="form-control" name="contas_bancarias[titular]" value="<?=@$r_conta->titular;?>"/>
					</div>
					<div class="form-group">
						<label>Documento:</label>
						<input type="text" class="form-control" name="contas_bancarias[documento]" value="<?=@$r_conta->documento;?>"/>
					</div>
					<div class="form-group">
						<label>Banco:</label>
						<input type="text" class="form-control" name="contas_bancarias[banco]" value="<?=@$r_conta->banco;?>"/>
					</div>
					<div class="form-group">
						<label>Agência:</label>
						<input type="text" class="form-control" name="contas_bancarias[agencia]" value="<?=@$r_conta->agencia;?>"/>
					</div>
					<div class="form-group">
						<label>Conta:</label>
						<input type="text" class="form-control" name="contas_bancarias[conta]" value="<?=@$r_conta->conta;?>"/>
					</div>		
					<div class="form-group">
						<label>Tipo:</label><br /><br />
						<input type="radio" name="contas_bancarias[tipo]" value="CC" <?=@$r_conta->tipo == 'CC' ? 'checked' : '';?>/>&nbsp;Conta Corrente&nbsp;&nbsp;
						<input type="radio" name="contas_bancarias[tipo]" value="P" <?=@$r_conta->tipo == 'P' ? 'checked' : '';?>/>&nbsp;Poupança&nbsp;&nbsp;
					</div>
					<div class="form-group text-right">	
						<button type="submit" class="btn btn-success">Salvar</button>
					</div>		
				</div>
			</div>
		</div>
	</form>	
	</div>
</div>

<!--
<div class="form-group text-right">
	<button type="submit" class="btn btn-success">Salvar</button>
</div>
-->

<?php
include 'footer.php';
?>
