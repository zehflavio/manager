<?php
include 'header.php';
include 'navbar.php';

$headers = ['Content-Type' => 'application/json', 'TokenLogado' => '$2y$10$jdtsD.LcdG4oFrXf6KES9OcynWl7Fm/N15qj2Z1MY27ri3Lf82ylm'];
$veiculos = Unirest\Request::get(ENDPOINT.'/Veiculos', $headers, null)->body->return;
?>
	<div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Veículos cadastrados
	                <div class="links-header pull-right">		                
		                <div>
		                    <a href="veiculos/formulario"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar</a>
		                </div>
		            </div>
	            </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Placa</th>                                        
                                        <th>Tipo</th>
                                        <th>Usuário</th>
                                        <th>Data</th>
                                        <th>Status</th>
                                        <th>Ações</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	<?php
                                	$i = 0;
                                	foreach ($veiculos as $key => $value) {
                                		$usuario = Unirest\Request::get(ENDPOINT.'/Usuarios/'.$veiculos->{$key}->usuarios_id, $headers, null)->body->usuario;
                                		switch ($veiculos->{$key}->tipo) {
                                			case 1:
                                				$tipo = 'Moto';
                            				break;
                                			case 2:
                                				$tipo = 'Carro';
                            				break;
                            				case 3:
                                				$tipo = 'Veículo Grande';
                            				break;
                                		}
                                	?>
                                		<tr class="<?=$i%2 == 0 ? 'odd' : 'even';?>">
	                                        <td><?=$veiculos->{$key}->placa;?></td>
                                            <td><?=$tipo;?></td>
	                                        <td><?=$usuario->nome;?></td>
	                                        <td><?=date('d/m/Y H:i', strtotime($veiculos->{$key}->dt_criacao));?></td>
                                            <td><?=$veiculos->{$key}->status == 0 ? 'Inativo' : 'Ativo';?></td>
	                                        <td class="text-center">
	                                        	<a href="veiculos/formulario/<?=$veiculos->{$key}->id;?>">
                                                    <button type="button" class="btn btn-sm btn-outline btn-info">Editar</button>
                                                </a>
                                                <?php if($veiculos->{$key}->status == 1){?>
                                                    <button   type="button" 
                                                            class="btn btn-sm btn-outline btn-danger excluir" 
                                                            data-entidade="veiculos" 
                                                            data-id="<?=$veiculos->{$key}->id;?>">Excluir</button>
                                                <?php } ?>
	                                        </td>
	                                    </tr>
                                	<?php
                                		$i++;
                                	}
                                    ?>
                                </tbody>
                            </table>
                        </div>                            
                    </div>                        
                </div>                    
            </div>                
        </div>
    </div>
<?php
include 'footer.php';
?>