<?php
include 'header.php';
include 'navbar.php';

$headers = ['Content-Type' => 'application/json', 'TokenLogado' => '$2y$10$jdtsD.LcdG4oFrXf6KES9OcynWl7Fm/N15qj2Z1MY27ri3Lf82ylm'];

if(!empty($_POST)){
	$data = $_POST;
	$body = Unirest\Request\Body::json($data);
	if(@$_GET['id']){
		$post = Unirest\Request::post(ENDPOINT.'/Eventos/update/'.$_GET['id'], $headers, $body);
	}else{
		$post = Unirest\Request::post(ENDPOINT.'/Eventos/', $headers, $body);
	}
	$return = json_decode($post->raw_body,TRUE);
}
if(isset($_GET['id'])){
	$edit = Unirest\Request::get(ENDPOINT.'/Eventos/'.$_GET['id'], $headers, null)->body->return;

	$estac   = Unirest\Request::get(ENDPOINT.'/Estacionamentos/'.$edit->estacionamentos_id, $headers, null)->body->return;
    $usuario = Unirest\Request::get(ENDPOINT.'/Usuarios/'.$edit->usuarios_id, $headers, null)->body->usuario;
    $veiculo = Unirest\Request::get(ENDPOINT.'/Veiculos/'.$edit->veiculos_id, $headers, null)->body->return;
    $param = Unirest\Request\Body::json(['eventos_id' => $edit->id]);
    $transacao = Unirest\Request::post(ENDPOINT.'/Pagamentos/Evento', $headers, $param)->body;
}
?>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				<?php
					echo !isset($_GET['id']) ? 'Adicionar novo' : 'Editar';
				?>
			</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6">
			<div class="panel panel-default">
				<?php
				if(isset($return)){
					if($return['status'] == false){
						echo '<div class="alert alert-danger"><strong>Erro!</strong> '.$return['return'].'</div>';
					}else{
						echo '<div class="alert alert-success"><strong>Sucesso!</strong> '.$return['return'].'</div>';
					}
				}
				?>
				<div class="panel-body panel-form">
					<form method="post" action="">
						<div class="form-group">
							<label>Data:</label>
							<input type="text" readonly class="form-control" value="<?=date('d/m/Y H:i:s', strtotime(@$edit->dt_inicio));?>" />
						</div>
						<div class="form-group">
							<label>Cliente:</label>
							<input type="text" readonly class="form-control" value="<?=@$usuario->nome;?>" />
						</div>
						<div class="form-group">
							<label>Veículo:</label>
							<input type="text" readonly class="form-control" required value="<?=@$veiculo->placa;?>" />
						</div>
						<div class="form-group">
							<label>Estacionamento:</label>
							<input type="text" readonly class="form-control" required value="<?=@$estac->nome;?>" />
						</div>
						<div class="form-group">
							<label>Valor:</label>
							<input type="text" readonly class="form-control" required value="R$ <?=number_format(@$transacao->valor, 2, ',', '');?>" />
						</div>
						<div class="form-group">
							<label>Status:</label>
							<select name="status" class="form-control">
								<option value="1" <?=$edit->status == 1 ? "selected" : "";?> > Aguardando estacionamento</option>
								<option value="2" <?=$edit->status == 2 ? "selected" : "";?> > Aguardando finalização</option>
								<option value="3" <?=$edit->status == 3 ? "selected" : "";?> > Finalizado</option>
								<option value="4" <?=$edit->status == 4 ? "selected" : "";?> > Cancelado pelo estacionamento</option>
								<option value="5" <?=$edit->status == 5 ? "selected" : "";?> > Cancelado pelo cliente</option>
                            </select>
						</div>
						<div class="form-group text-right">
							<button type="submit" class="btn btn-success">Salvar</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>



<?php
include 'footer.php';
?>